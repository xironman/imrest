# imrest 

imrest is a RESTful Web Server based on Mongoose that provides encrypted messaging between Ironman sensors, Ironman Jarvis voice control and the Ironman monitor that runs on a PiBox server.

imrest runs as HTTP (not HTTPS) using a JSON packet that contains an encrypted message.  While the JSON packet can be intercepted, the encrypted messages should be difficult to break.  The reason for using HTTP is because some Ironman sensors, such as those based on the ESP01 (re: ESP8266), cannot support HTTPS messaging but can perform simpler encryption manually.  For the small sized messages that are exchanged this is sufficient.

# Ironman REST interface

The format for REST requests is

|Method |API                  |From ...                |Description                                 |
|-------|---------------------|------------------------|--------------------------------------------|
|GET    |/query/devices       |From monitor            |Request Device states from paired IoT nodes |
|GET    |/monitor             |From Jarvis             |Retrieve monitor description file           |
|POST   |/pair/iot/<uuid>     |From IoT Node           |Pair an IoT node with the monitor           |
|POST   |/pair/jarvis/<uuid>  |From Jarvis             |Pair a Jarvis instance with the monitor     |
|POST   |/devices             |From Jarvis             |Get list of IoT nodes                       |
|POST   |/set/device          |From Jarvis             |Change an IoT node state                    |
|POST   |/ping                |From IoT Node           |Verify IoT node registration.               |
|DELETE |/set/device          |From Jarvis or monitor  |Change an IoT node state                    |

Version is added to header, as such:
* Accept-Version: 1.0

Additionally, JSON data may be included.  This would be data specific to the cmd.

