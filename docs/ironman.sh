#!/bin/bash -p
# Functions and aliases used to nagivate project directories.
# Include by the ~/bin/cdtools script.
#############################################################################

# -------------------------------------------------------------------
# DESC: Ironman projects overlaying PiBox Development Platform
# Edit variables with <> values, as needed.
# -------------------------------------------------------------------
function ironman {
    # -------------------------------------------------------------------
    # Edit these items
    # -------------------------------------------------------------------

    # Top of the source tree
    # The project directory will be placed under SRCTOP
    # SRCTOP=<PATH TO WHERE PROJECT DIRECTORY WILL LIVE>
    SRCTOP=/home/mjhammel/src/ximba/ironman

    # -------------------------------------------------------------------
    # Don't edit below here
    # -------------------------------------------------------------------

    # Project ID
    PARENT=xironman

    # Project ID:  Defaults to "monitor" unless first argument is set
    if [ "$1" != "" ]
    then
        case "$1" in
        "monitor")       PRJ=$1;;
        "gpio")          PRJ=$1;;
        "contacts")      PRJ=$1;;
        "sensors")       PRJ=$1;;
        "ironmancfg")    PRJ=ironmancfg;;
        "daemon")        PRJ=$1;;
        "launcher")      PRJ=$1;;
        "www")           PRJ=$1;;
        "imrest")        PRJ=$1;;
        "mobile")        PRJ=$1;;
        "meta")          PRJ=$1;;
        *) 
            echo "Invalid repo"
            ironmanrepos
            return 0
            ;;
        esac
    else
        PRJ=monitor
    fi

    # GIT Repo
    export GITREPO=git@gitlab.com:$PARENT/$PRJ.git

    # Suffix allows for creating multiple trees for the same repo
    if [ "$1" != "" ]
    then
        SFX=$2
    else
        SFX=$1
    fi

    # Create top level directory, if needed
    mkdir -p $SRCTOP

    # Where I do my dev work
    GM_WORK=$SRCTOP/work
    mkdir -p $GM_WORK

    # Where the SCM is located
    GM_HOME=$SRCTOP/$PRJ$SFX

    # Where the source, build and package directories live
    GM_SRC=$GM_HOME/src
    GM_BUILD=$GM_HOME/bld
    GM_PKG=$GM_HOME/pkg
    GM_ARCHIVE=$GM_HOME/archive
    GM_EXTRAS=$GM_HOME/extras

    # Make the configured environment available 
    export GM_SRC
    export GM_BUILD
    export GM_PKG
    export GM_ARCHIVE
    export GM_EXTRAS
    export GM_WORK
    export GM_HOME

    # Some aliases to bounce around directories easily
    alias cdt='cd $SRCTOP'
    alias cdh='cd $GM_HOME'
    alias cdw='cd $GM_WORK'
    alias cdx='cd $GM_SRC'
    alias cdb='cd $GM_BUILD'
    alias cdp='cd $GM_PKG'
    alias cde='cd $GM_EXTRAS'
    alias cdl='ironmanrepos'

    # Show the aliases for this configuration
    alias cd?='listironman'
}
function listironman {
echo "
$PARENT $PRJ Alias settings:
-----------------------------------------------------------------------------
cdt    cd SRCTOP ($SRCTOP)
cdh    cd GM_HOME ($GM_HOME)
cdw    cd GM_WORK ($GM_WORK)
cdx    cd GM_SRC ($GM_SRC)
cdb    cd GM_BUILD ($GM_BUILD)
cdp    cd GM_PKG ($GM_PKG)
cde    cd GM_EXTRAS ($GM_EXTRAS)
cdl    List available repositories

To checkout tree:
cdt
mkdir $PRJ$SFX
cdh
git clone $GITREPO src

Pushing your local repository to Gitorious:
    cd <src>
    git init
    git add .
    git commit
    git checkout master
    git remote add origin $GITREPO
    git push origin master
"
}
function ironmanrepos {
echo "
$PRJ repos:
--------------------------------------------------------------------------------
monitor:    Updates to PiBox Development Platform to create Ironman Monitor core
gpio:       GPIO utility for setting and reading GPIO pins on the Raspberry Pi
contacts:   Contact backend - manage lists for sending notifications
sensors:    Sensor backend - Browse and manage sensors and sensor groups from monitor
ironmancfg: Administrative backend - manage admin pw, keys (aka ironmancfg)
daemon:     Daemons used in Ironman project
launcher:   Home Automation UI for monitor
www:        Web extensions - wifi/ap, contacts, sensors, files, setup
imrest:     RESTful Web - used for sensors/Jarvis interaction with Ironman
mobile:     Android app - wifi/ap, contacts, sensors, files
meta:       Metabuilder - builds all PiBox add-on packages associated with Ironman for distribution.
"
}



