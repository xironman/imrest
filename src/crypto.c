/*******************************************************************************
 * imrest - a RESTful web server based on Mongoose.
 *
 * crypto.c:  Encryption functions
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define CRYPTO_C

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <uuid.h>
#include <sys/types.h>

#include "imrest.h"

/*========================================================================
 *========================================================================
 * STATIC FUNCTIONS
 *========================================================================
 *========================================================================*/

/*========================================================================
 * Name:   cryptoDoDecode
 * Prototype:  char *cryptoDoDecode( char *iv, char *cryptKey, char *message )
 *
 * Description:
 * Decode a message from it's IV and crypt key.
 * This first decodes the Initializing Vector (re: iv) and then message from base64,
 * then decrypts the message using the IV using AES128 CBC.
 *
 * Input Arguments:
 * char *iv             Input vector that changes for each message.
 * char *cryptKey       Crypto key for the sensor or host.
 * char *message        Message to be decrypted.
 *
 * Returns:
 * The messages as a character string or NULL if the message cannot be decrypted.
 * Caller is responsible for freeing the returned string.
 *
 * Notes:
 * See https://github.com/superwills/NibbleAndAHalf/tree/master/NibbleAndAHalf
 * See https://github.com/kokke/tiny-AES-c
 *========================================================================*/
static char *
cryptoDoDecode( char *iv, char *cryptKey, char *message )
{
    unsigned char   *ivbytes = NULL;
    int             ivbytesLen;
    unsigned char   *encryptedText = NULL;
    char            *decryptedText = NULL;
    int             encryptedTextLen;
    struct AES_ctx  ctx;
    size_t          actualDataLength;
    char            *ptr;

    /* Decode IV from base64. When decoded, it must be 16 bytes for AES128 CBC decryption. */
    piboxLogger(LOG_INFO, "Decoding IV.\n");
    ivbytes = unbase64( iv, strlen(iv), &ivbytesLen );
    if ( ivbytes == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed decoding IV.\n");
        goto cryptoDoDecode_exit;
    }
    piboxLogger(LOG_INFO, "Decoded IV length: %d bytes\n", ivbytesLen);

    /* Decode message from base64.  Save a pointer to it for clarity. */
    piboxLogger(LOG_INFO, "Decoding message.\n");
    encryptedText = unbase64( message, strlen(message), &encryptedTextLen );
    if ( encryptedText == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed decoding IV.\n");
        goto cryptoDoDecode_exit;
    }
    decryptedText = (char *)encryptedText;
    piboxLogger(LOG_INFO, "Decoded message length: %d bytes\n", encryptedTextLen);

    /* 
     * decrypt message: decrypt function decrypts the message in-place.
     */
    piboxLogger(LOG_INFO, "Initializing AES.\n");
    AES_init_ctx_iv(&ctx, (const uint8_t *)cryptKey, (const uint8_t *)ivbytes);
    piboxLogger(LOG_INFO, "Decrypting message.\n");
    AES_CBC_decrypt_buffer(&ctx, (uint8_t *)decryptedText, (size_t)(encryptedTextLen));

    actualDataLength = pkcs7_padding_data_length( (uint8_t *)decryptedText, encryptedTextLen, 16);
    piboxLogger(LOG_INFO, "actualDataLength: %d bytes\n", actualDataLength);
    if ( actualDataLength == 0 )
    {
        /* Error - decryption failed. */
        piboxLogger(LOG_ERROR, "Decrypted message failed padding test.\n");
        free(decryptedText);
        decryptedText = NULL;
        goto cryptoDoDecode_exit;
    }
    if ( actualDataLength == encryptedTextLen )
    {
        /* Allocate new buffer + 1 to NULL terminate */
        piboxLogger(LOG_INFO, "Allocting new buffer to NULL teriminate.\n");
        ptr = (char *)calloc(1, encryptedTextLen + 1);
        memcpy(ptr, decryptedText, encryptedTextLen);
        free(decryptedText);
        decryptedText = ptr;
    }
    else
    {
        /*
         * Actual data is smaller than decryptedText buffer
         * length, so NULL terminate in place.
         */
        piboxLogger(LOG_INFO, "In place NULL terimination.\n");
        memset((char *)(decryptedText+actualDataLength), 0, 1);
    }

    piboxLogger(LOG_INFO, "Decrypted message: %s\n", decryptedText);

cryptoDoDecode_exit:
    if ( ivbytes )
        free(ivbytes);

    return (decryptedText);
}

/*========================================================================
 *========================================================================
 * PUBLIC FUNCTIONS
 *========================================================================
 *========================================================================*/

/*========================================================================
 * Name:   cryptoEncrypt
 * Prototype:  char *cryptoEncrypt( char *key, char *message )
 *
 * Description:
 * Front end to encrypting a message.  Message is encrypted using key, then
 * encoded using base64, along with a generated IV and returned in a JSON
 * packet.
 *
 * Input Arguments:
 * char *key            UUID to use for encrypting - only 16 bytes are used.
 * char *message        Message to encrypt.
 *
 * Returns:
 * JSON string of the form:
 * {
 *   "iv": "<Initialization vector string>",
 *   "message": "<Encrypted message>"
 * }
 * Both iv and message are base64 encoded.
 *
 * Notes:
 * See https://github.com/kokke/tiny-AES-c
 * See https://en.wikipedia.org/wiki/Padding_(cryptography)#PKCS#5_and_PKCS#7
 *========================================================================*/
char *
cryptoEncrypt( char *key, char *message )
{
    char            *cryptKey;
    char            *ivbytes;
    char            *msg;
    char            *json;
    char            *encodedText;
    int             encodedTextLen;
    char            *encodedIV;
    int             encodedIVLen;
    int             msgLen;
    int             padLen;
    uuid_t          uuid;
    struct AES_ctx  ctx;
    JSON_Value      *root_value;     // Data block wrapping JSON objects
    JSON_Object     *root_object;    // root JSON object

    piboxLogger(LOG_INFO, "Entered.\n");
    piboxLogger(LOG_INFO, "message: %s\n", message);

    cryptKey = strdup(key);

    /* Key must be 16 bytes */
    if ( strlen(cryptKey) < 16 )
    {
        piboxLogger(LOG_ERROR, "Crypto key is too short.\n");
        free(cryptKey);
        return NULL;
    }
    if ( strlen(cryptKey) > 16 )
    {   
        memcpy((char *)(cryptKey+16), "\0", 1);
    }
    piboxLogger(LOG_INFO, "cryptKey: %s\n", cryptKey);

    /* IV must be 16 bytes */
    uuid_generate(uuid);
    ivbytes = (char *)calloc(1,37);
    uuid_unparse(uuid, ivbytes);
    memcpy((char *)(ivbytes+16), "\0", 1);

    /* Update message with PKCS padding */
    msgLen = strlen(message);
    if ( msgLen % AES_BLOCKLEN )
        msgLen += 16 - (msgLen % 16);
    msg = (char *)calloc(1, msgLen);
    memcpy(msg, message, strlen(message));
    padLen = pkcs7_padding_pad_buffer( (uint8_t *)msg, strlen(message), msgLen, AES_BLOCKLEN );
    piboxLogger(LOG_INFO, "Original message is %d bytes.\n", strlen(message));
    piboxLogger(LOG_INFO, "Padded message buffer with %d bytes.\n", padLen);
    piboxLogger(LOG_INFO, "Total bytes is %d.\n", msgLen);

    /* Encrypt the message using the IV. */
    AES_init_ctx_iv(&ctx, (const uint8_t *)cryptKey, (const uint8_t *)ivbytes);
    AES_CBC_encrypt_buffer(&ctx, (uint8_t *)msg, (size_t)(msgLen));

    /* Encode to base64 both the IV and encrypted message */
    encodedText = base64( msg, msgLen, &encodedTextLen );
    encodedIV = base64( ivbytes, 16, &encodedIVLen );

    /* Build JSON package for return to caller. */
    root_value = json_value_init_object();
    root_object = json_value_get_object(root_value);
    json_object_set_string( root_object, "iv", encodedIV );
    json_object_set_string( root_object, "message", encodedText );
    json = json_serialize_to_string(root_value);
    json_value_free(root_value);

    /* Cleanup */
    free(encodedIV);
    free(encodedText);
    free(msg);
    free(ivbytes);
    free(cryptKey);

    /* Return JSON - caller must free buffer */
    piboxLogger(LOG_INFO, "Encrypted JSON: %s\n", json);
    return (json);
}

/*========================================================================
 * Name:   cryptoDecrypt
 * Prototype:  char *cryptoDecrypt( char *src, char *json, char *ipaddr )
 *
 * Description:
 * Front end to decrypting a message based on request data.
 *
 * Input Arguments:
 * char *src            either "jarvis" (voice control) or "iot" (sensor device)
 * char *json           JSON packet with IV and message to decrypt.
 * char *ipaddr         IP address of remote host that sent the message.
 *                      Used to find decryption key.
 *
 * Returns:
 * The decrypted string or NULL if the message cannot be decrypted.
 *
 * Notes:
 * https://github.com/kokke/tiny-AES-c
 *========================================================================*/
char *
cryptoDecrypt( char *src, char *json, char *ipaddr )
{
    char            *cryptKey;
    char            *iv;
    char            *msg;
    char            *uuid;
    char            *response = NULL;
    JSON_Value      *root_value;     // Data block wrapping JSON objects
    JSON_Object     *root_object;    // root JSON object

    piboxLogger(LOG_INFO, "Entered.\n");

    /* Retrieve the cryptKey for the specified device/host. */
    uuid = utilsLoadUUID(src, ipaddr);
    if ( uuid == NULL )
    {
        piboxLogger(LOG_ERROR, "Can't find uuid for node: %s\n", ipaddr);
        return NULL;
    }

    cryptKey = strdup(uuid);
    free(uuid);

    /* Key must be 16 bytes */
    if ( strlen(cryptKey) < 16 )
    {
        piboxLogger(LOG_ERROR, "Crypto key is too short.\n");
        free(cryptKey);
        return NULL;
    }
    if ( strlen(cryptKey) > 16 )
    {   
        memcpy((char *)(cryptKey+16), "\0", 1);
    }
    piboxLogger(LOG_INFO, "cryptKey: %s\n", cryptKey);

    /* Get IV and message from JSON */
    root_value = json_parse_string(json);
    root_object = json_value_get_object(root_value);
    iv = (char *)json_object_get_string( root_object, "iv" );
    msg = (char *)json_object_get_string( root_object, "message" );
    if ( (iv!=NULL) && (msg!=NULL) )
    {
        piboxLogger(LOG_INFO, "iv: %s\n", iv);
        piboxLogger(LOG_INFO, "message: %s\n", msg);
        piboxLogger(LOG_INFO, "message length: %d\n", strlen(msg));
        piboxLogger(LOG_INFO, "iv length: %d\n", strlen(iv));
        piboxLogger(LOG_INFO, "cryptKey length: %d\n", strlen(cryptKey));

        /* Decrypt and return it. */
        response = cryptoDoDecode(iv, cryptKey, msg);
        free(cryptKey);

        piboxLogger(LOG_INFO, "Response: %s\n", response);
    }
    json_value_free(root_value);
    return (response);
}

/*========================================================================
 * Name:   cryptoDecryptJson
 * Prototype:  char *cryptoDecryptJson( char *key, char *json )
 *
 * Description:
 * Front end to decrypting a message based on uuid and a JSON response with iv and message.
 * Returns the decrypted string.
 *
 * Input Arguments:
 * char *key        UUID (aka crypt key) for sending sensor/host.
 * char *json       JSON packet containing IV and encoded/encrypted message.
 *
 * Returns:
 * The decoded/decrypted message as a string or NULL if message can't be
 * decoded/decrypted.  Caller is responsible for freeing allocated string.
 *
 * Notes:
 * JSON data length must be a multiple of AES_BLOCKLEN (re: length%AES_BLOCKLEN == 0 )
 * Cryptkey must be 16 bytes.  It will be truncated if too long.
 *========================================================================*/
char *
cryptoDecryptJson( char *key, char *json )
{
    char            *cryptKey;
    char            *iv;
    char            *msg;
    char            *response;
    JSON_Value      *root_value;     // Data block wrapping JSON objects
    JSON_Object     *root_object;    // root JSON object

    cryptKey = strdup(key);
    if ( strlen(cryptKey) < 16 )
    {   
        piboxLogger(LOG_ERROR, "Crypto key is too short.\n");
        free(cryptKey);
        return NULL;
    }
    if ( strlen(cryptKey) > 16 )
    {   
        memcpy((char *)(cryptKey+16), "\0", 1);
    }
    piboxLogger(LOG_INFO, "cryptKey: %s\n", cryptKey);

    /* Get IV and encoded/encrypted message from request */
    root_value = json_parse_string(json);
    if ( root_value == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to parse json string: %s\n", json);
        return NULL;
    }
    root_object = json_value_get_object(root_value);
    iv = (char *)json_object_get_string( root_object, "iv" );
    msg = (char *)json_object_get_string( root_object, "message" );
    piboxLogger(LOG_INFO, "iv: %s\n", iv);
    piboxLogger(LOG_INFO, "message: %s\n", msg);

    /* Decrypt and return it. */
    response = cryptoDoDecode(iv, cryptKey, msg);

    json_value_free(root_value);
    free(cryptKey);
    return (response);
}
