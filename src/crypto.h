/*******************************************************************************
 * imrest - a RESTful web server based on Mongoose.
 *
 * crypto.h:  Utilty functions
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef CRYPTO_H
#define CRYPTO_H

/*========================================================================
 * Defined values
 *=======================================================================*/

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef CRYPTO_C
extern char *cryptoDecryptJson( char *key, char *json );
extern char *cryptoDecrypt( char *src, char *json, char *ipaddr );
extern char *cryptoEncrypt( char *key, char *message );
#endif /* !CRYPTO_C */
#endif /* !CRYPTO_H */
