/*******************************************************************************
 * imrest - a RESTful web server based on Mongoose.
 *
 * device.c:  Sensor device handling functions
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define DEVICE_C

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <uuid.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "imrest.h"

/*========================================================================
 *========================================================================
 * STATIC FUNCTIONS
 *========================================================================
 *========================================================================*/

/*========================================================================
 * Name:   deviceSaveSensorData
 * Prototype:  void deviceSaveSensorData( char *buffer, char *userdata )
 *
 * Description:
 * Inbound data is sent here by curl.  But a single response may not be a complete body of a single response.
 * So this function builds the response over one or more calls from curl.
 *
 * Input Arguments:
 * char *json       Return data from an HTTP call to a sensor. This is not NULL terminated.
 * void *ipaddr     Storage for the inbound response.
 *
 * Returns:
 * N/A
 *
 * Notes:
 * N/A
 *========================================================================*/
void
deviceSaveSensorData( char *json, char *ipaddr )
{
    char            *stampDir;
    char            *uuid;
    char            *message = NULL;
    char            *filename = NULL;
    FILE            *fd;

    piboxLogger(LOG_INFO, "Entered.\n");
    if ( json == NULL )
    {
        piboxLogger(LOG_INFO, "json == NULL.\n");
        return;
    }

    /* Grab the UUID of the sensor providing the data. */
    uuid = utilsLoadUUID("iot", ipaddr);
    if ( uuid == NULL )
    {
        piboxLogger(LOG_INFO, "Skipping response from unrecognized sensor: %s\n", ipaddr);
        return;
    }

    /* Decode the response */
    message = cryptoDecryptJson(uuid, json);
    if ( message == NULL )
    {
        piboxLogger(LOG_INFO, "Unable to decrypt message from %s\n", ipaddr);
        free(uuid);
        return;
    }

    /* Save the response */
    stampDir = initGetIotDir();
    filename = (char *)calloc(1, strlen(stampDir) + strlen(ipaddr) + 2 );
    sprintf(filename, "%s%s",  stampDir, ipaddr);
    piboxLogger(LOG_INFO, "Writing to sensor file %s\n", filename);

    fd = fopen(filename, "w");
    if ( fd != NULL )
    {
        if ( fwrite(message, 1, strlen(message), fd) != strlen(message) )
            piboxLogger(LOG_INFO, "Failed to write stamp file: %s\n", filename);
        else
            piboxLogger(LOG_INFO, "Updated device %s with %s\n", filename, message);
        fclose(fd);
    }
    else
        piboxLogger(LOG_INFO, "Failed to open for write: %s, reason: %s\n", filename, strerror(errno));

    free(filename);
    free(stampDir);
    free(message);
    free(uuid);
}

/*========================================================================
 *========================================================================
 * PUBLIC FUNCTIONS
 *========================================================================
 *========================================================================*/

/*========================================================================
 * Name:   deviceUpdate
 * Prototype:  void deviceUpdate( struct mg_connection *c, char *remoteIP, char *json )
 *
 * Description:
 * Process a Jarvis request to update a device state.  This means changing
 * the configuration of a device such as turning a light on or off or 
 * opening or closing a window blind, and so forth.
 *
 * Input Arguments:
 * struct mg_connection *c          Mongoose connection structure.
 * char                 *remoteIP   IP of node that made this request to us.
 * char                 *json       Encoded/encrypted inbound request.
 *
 * HTTP Response:
 * 200 if all goes well.
 * 400 if message cannot be processed.
 * 401 if request cannot be authorized
 * 500 if request cannot be processed
 *
 * Notes:
 * This function accepts the Jarvis request and converts it to a device
 * request, then makes the request to the device.
 * The encrypted message field of the json argument, after decoding and decrypting,
 * will be a JSON string with the following fields:
 * 1. uuid field    - The UUID of the sensor node to update (uuid:<>)
 * 2. state field   - The new state of the sensor (state:<>).
 * 3. command field - The command, which must be "update" ( message:"update")
 *========================================================================*/
void
deviceUpdate( struct mg_connection *c, char *remoteIP, char *json )
{
    JSON_Value      *root_value;     // Data block wrapping JSON objects
    JSON_Object     *root_object;    // root JSON object
    JSON_Value      *update_value;   // Data block wrapping JSON objects
    JSON_Object     *update_object;  // root JSON object
    char            *cmd = NULL;
    char            *uuid = NULL;
    char            *state = NULL;
    char            *response = NULL;
    char            *crypt_json = NULL;
    char            *ipaddr = NULL;
    char            *message = NULL;
    char            *str = NULL;

    /* Don't decrypt if from the local machine. */
    if ( (strcmp(remoteIP, LOCALHOST)==0) || utilsLocalIP(remoteIP) )
    {   
        /* pisensors will send this as properly formatted JSON, but as a string. */
        piboxLogger(LOG_INFO, "Request rawBody from localhost: %s\n", json);
        message = strdup(json);
    }
    else
    {   
        message = cryptoDecrypt("jarvis", json, remoteIP);
        if ( message == NULL )
        {
            piboxLogger(LOG_ERROR, "Request from unrecognized client: %s\n", remoteIP);
            mg_http_reply(c, 401, "", "");
            return;
        }
        else
            piboxLogger(LOG_INFO, "Request params from jarvis: %s\n", message);
    }

    /* Verify the request contains the "update" command. */
    root_value = json_parse_string(message);
    if ( root_value == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to parse message: %s\n", message);
        free(message);
        goto deviceUpdate_exit;
    }
    root_object = json_value_get_object(root_value);
    free(message);
    str = json_serialize_to_string(root_value);
    piboxLogger(LOG_INFO, "Request: %s\n", str);
    json_free_serialized_string(str);

    cmd = (char *)json_object_get_string( root_object, "command" );
    if ( (cmd == NULL) || (strcmp(cmd, "update") != 0) )
    {
        piboxLogger(LOG_ERROR, "Bad request - request doesn't match update.\n");
        mg_http_reply(c, 400, NULL, "");
        c->is_closing = 1;
        json_value_free(root_value);
        goto deviceUpdate_exit;
    }
    piboxLogger(LOG_INFO, "Request command: %s\n", cmd);

    /* Verify the request contains the "uuid" key. */
    uuid = (char *)json_object_get_string( root_object, "uuid" );
    if ( uuid == NULL )
    {
        piboxLogger(LOG_ERROR, "Bad request - request doesn't include remote sensor id.\n");
        mg_http_reply(c, 400, "", "");
        json_value_free(root_value);
        goto deviceUpdate_exit;
    }
    piboxLogger(LOG_INFO, "Request uuid: %s\n", uuid);

    /* Verify the request contains the "state" key. */
    state = (char *)json_object_get_string( root_object, "state" );
    if ( state == NULL )
    {
        piboxLogger(LOG_ERROR, "Bad request - request doesn't include new state.\n");
        mg_http_reply(c, 400, NULL, "");
        json_value_free(root_value);
        goto deviceUpdate_exit;
    }
    piboxLogger(LOG_INFO, "Request state: %s\n", state);

    /* Find IPv4 of specified device */
    ipaddr = utilsGetIPv4(uuid);
    if ( ipaddr == NULL )
    {
        piboxLogger(LOG_ERROR, "Unauthorized: Update request does not match any paired devices.\n");
        mg_http_reply(c, 401, "", "");
        json_value_free(root_value);
        goto deviceUpdate_exit;
    }
    piboxLogger(LOG_INFO, "Request ipaddr: %s\n", ipaddr);

    /* Build an update packet */
    update_value = json_value_init_object();
    update_object = json_value_get_object(update_value);
    json_object_set_string( update_object, "state", state );

    response = json_serialize_to_string(update_value);
    json_value_free(update_value);
    piboxLogger(LOG_INFO, "Response: %s\n", response);
    crypt_json = cryptoEncrypt(uuid, response);
    free(response);
    if ( crypt_json != NULL )
        piboxLogger(LOG_INFO, "%s\n", crypt_json);
    else
    {
        piboxLogger(LOG_ERROR, "Failed to encrypt for %s\n", ipaddr );
        mg_http_reply(c, 500, "", "");
        json_value_free(root_value);
        goto deviceUpdate_exit;
    }

    /* Send it to the device and update the local configuration file */
    piboxLogger(LOG_INFO, "Contacting %s\n", ipaddr );
    utilsHttpPost(ipaddr, "/set", crypt_json);

    /* All is well.  Return response from device, if any. */
    mg_http_reply(c, 200, "", "");

    json_value_free(root_value);

deviceUpdate_exit:

    /* Cleanup */
    piboxLogger(LOG_INFO, "Freeing state\n");
    piboxLogger(LOG_INFO, "Freeing crypt_json\n");
    if ( crypt_json != NULL )
        free(crypt_json);
    piboxLogger(LOG_INFO, "Freeing ipaddr\n");
    if ( ipaddr != NULL )
        free(ipaddr);
}

/*========================================================================
 * Name:   deviceGetDevices
 * Prototype:  void deviceGetDevices( struct mg_connection *c, char *remoteIP )
 *
 * Description:
 * Get status of all devices.
 * This request must come via a local (loopback) query (not from a Jarvis node).
 * The monitor queries for an update of device state from all paired devices.
 *
 * Input Arguments:
 * struct mg_connection *c          Mongoose connection structure.
 * char                 *remoteIP   IP of node that made this request to us.
 *
 * HTTP Response:
 * 200 if all goes well.
 * 401 if request cannot be authorized
 * 500 if request cannot be processed
 *
 * Notes:
 * This request does not require a JSON body.
 *========================================================================*/
void
deviceGetDevices( struct mg_connection *c, char *remoteIP )
{
    char            *iotDir = NULL;
    DIR             *pdir = NULL;
    struct dirent   *pent = NULL;
    char            *uuid = NULL;
    char            *crypt_json = NULL;
    int             rc = 200;

    piboxLogger(LOG_INFO, "Entered.\n");

    /* Only allow these requests from the local machine. */
    if ( ! ((strcmp(remoteIP, LOCALHOST)==0) || utilsLocalIP(remoteIP)) )
    {
        piboxLogger(LOG_INFO, "GetDevices request is not from local system.  Ignoring %s\n", remoteIP);
        mg_http_reply(c, 401, "", "");
        return;
    }
    piboxLogger(LOG_INFO, "GetDevices request from local system.\n");

    /* Iterate all paired IoT devices. */
    iotDir = initGetIotDir();
    if ( iotDir == NULL )
    {
        piboxLogger(LOG_ERROR, "Can't open IoT dir.");
        mg_http_reply(c, 500, "", "");
        return;
    }

    /* Open iotDir */
    piboxLogger(LOG_INFO, "iotDir: %s\n", iotDir);
    pdir = opendir( iotDir );
    if (pdir != NULL)
    {
        /* Iterate files */
        while( (pent = readdir(pdir)) != NULL )
        {   
            /* Skip non-sensor-device entries */
            if ( strncmp(pent->d_name, ".", 1) == 0 )
                continue;

            /* Call utilsLoadUUID() to read UUID from file */
            piboxLogger(LOG_INFO, "Requesting check of %s\n", pent->d_name);
            uuid = utilsLoadUUID("iot", pent->d_name);
            if ( uuid == NULL )
            {
                piboxLogger(LOG_ERROR, "Failed check of %s\n", pent->d_name);
                continue;
            }

            /* Encrypt the request - this helps device verify we're the correct monitor. */
            crypt_json = cryptoEncrypt(uuid, "getdevices");
            free(uuid);
            uuid = NULL;

            if ( crypt_json == NULL )
            {
                piboxLogger(LOG_ERROR, "Failed to encrypt getdevices for %s.", remoteIP);
                rc = 500;
                break;
            }

            /* Query the device for updated information.  The filename is it's IP address. */
            piboxLogger(LOG_INFO, "Contacting %s\n", pent->d_name );
            utilsHttpPost(pent->d_name, "/query", crypt_json);

            free(crypt_json);
            crypt_json = NULL;
        }
        closedir( pdir );
    }
    else
    {
        piboxLogger(LOG_INFO, "Can't open iotdir: %s\n", iotDir);
        rc = 500;
    }

    /* All is well, as far as we know. */
    mg_http_reply(c, rc, "", "");

    /* Clean up */
    if ( crypt_json != NULL )
        free(crypt_json);
    if ( uuid != NULL )
        free(uuid);
    if ( iotDir != NULL )
        free(iotDir);
}

/*========================================================================
 * Name:   deviceListDevices
 * Prototype:  void deviceListDevices( struct mg_connection *c, char *remoteIP, char *json )
 *
 * Description:
 * Get a list of devices, returned as JSON to the Jarvis node that made the request.
 *
 * Input Arguments:
 * struct mg_connection *c          Mongoose connection structure.
 * char                 *remoteIP   IP of node that made this request to us.
 * char                 *body       Inbound request.
 *
 * HTTP Response:
 * 200 if all goes well.
 * 400 if message cannot be processed.
 * 401 if request cannot be authorized
 *
 * Notes:
 * TBD
 *========================================================================*/
void
deviceListDevices( struct mg_connection *c, char *remoteIP, char *body )
{
    char            *json = NULL;
    char            *message = NULL;
    char            *cmd = NULL;
    char            *iotDir = NULL;
    DIR             *pdir = NULL;
    char            *uuid = NULL;
    char            *jarvis_uuid = NULL;
    char            *crypt_json = NULL;
    char            *filename = NULL;
    char            *buf = NULL;
    struct dirent   *pent = NULL;
    JSON_Value      *root_value;
    JSON_Object     *root_object;
    JSON_Value      *devices_value;
    JSON_Array      *devices_array;
    JSON_Value      *read_value;
    int             rc = 200;
    struct stat     stat_buf;
    FILE            *fd;

    piboxLogger(LOG_INFO, "Entered.\n");

    /* Get the Jarvis UUID, if registered. */
    jarvis_uuid = utilsLoadUUID("jarvis", remoteIP);
    if ( jarvis_uuid == NULL )
    {
        piboxLogger(LOG_ERROR, "Request from unrecognized client: %s\n", remoteIP);
        rc = 401;
        goto deviceListDevices_exit;
    }

    message = cryptoDecrypt("jarvis", body, remoteIP);
    if ( message == NULL )
    {
        piboxLogger(LOG_ERROR, "Decryption failed for message: %s\n", remoteIP);
        rc = 401;
        goto deviceListDevices_exit;
    }
    piboxLogger(LOG_INFO, "Received POST /devices request; message = %s\n", message);

    /* Verify the request contains the "delete" command. */
    root_value = json_parse_string(message);
    root_object = json_value_get_object(root_value);
    free(message);

    cmd = (char *)json_object_get_string( root_object, "command" );
    if ( (cmd == NULL) || (strcmp(cmd, "getDevices") != 0) )
    {
        piboxLogger(LOG_ERROR, "Bad request - request doesn't match update.\n");
        mg_http_reply(c, 400, NULL, "");
        c->is_closing = 1;
        json_value_free(root_value);
        goto deviceListDevices_exit;
    }
    piboxLogger(LOG_INFO, "Request command: %s\n", cmd);
    json_value_free(root_value);

    /* Iterate over directory to pull in each device configuration. */
    iotDir = initGetIotDir();

    pdir = opendir( iotDir );
    if (pdir != NULL)
    {
        uuid = initGetUUID();
        root_value = json_value_init_object();
        root_object = json_value_get_object(root_value);
        json_object_set_string( root_object, "monitor", uuid );
        free(uuid);
        devices_value = json_value_init_array();
        devices_array = json_value_get_array(devices_value);
        json_object_set_value(root_object, "devices", devices_value);

        /* Iterate files */
        while( (pent = readdir(pdir)) != NULL )
        {   
            /* Skip non-sensor-device entries */
            if ( strncmp(pent->d_name, ".", 1) == 0 )
                continue;

            /* Build full file name with path */
            filename = (char *)calloc(1, strlen(iotDir) + strlen(pent->d_name) + 2 );
            sprintf(filename, "%s/%s", iotDir, pent->d_name);
            stat(filename, &stat_buf);
            if ( stat_buf.st_size == 0 )
            {
                free(filename);
                continue;
            }

            /* Read the file */
            fd = fopen(filename, "r");
            if ( !fd )
            {
                free(filename);
                continue;
            }
            buf = (char *)calloc(1, stat_buf.st_size);
            if ( fread(buf, 1, stat_buf.st_size, fd) != stat_buf.st_size )
            {
                free(filename);
                free(buf);
                continue;
            }

            read_value = json_parse_string(buf);
            json_array_append_value(devices_array, read_value);
            free(filename);
            free(buf);
        }
        closedir(pdir);

        json = json_serialize_to_string(root_value);
        json_value_free(root_value);
        piboxLogger(LOG_INFO, "Device list: %s\n", json);

        /* Encrypt the response */
        crypt_json = cryptoEncrypt(jarvis_uuid, json);
        if ( crypt_json == NULL )
            rc = 400;
        else
            piboxLogger(LOG_INFO, "Response has been encrypted: %s\n", crypt_json);
        json_free_serialized_string(json);
    }
    else
    {
        piboxLogger(LOG_ERROR, "Can't open iotDir: %s\n", iotDir);
    }

deviceListDevices_exit:
    if ( crypt_json )
        mg_http_reply(c, rc, "", "%s\n", crypt_json);
    else
        mg_http_reply(c, rc, "", "");

    /* Clean up */
    if ( crypt_json != NULL )
        free(crypt_json);
    if ( iotDir != NULL )
        free(iotDir);
    if ( jarvis_uuid != NULL )
        free(jarvis_uuid);
}

/*========================================================================
 * Name:   devicePing
 * Prototype:  void devicePing( struct mg_connection *c, char *remoteIP, char *json )
 *
 * Description:
 * Handle a device ping. This is a device powering up that thinks it
 * has previously been registered.  If we can decode it's request then
 * it is registered and we return 200.  If we can't decode it, then
 * we return 401.
 *
 * Input Arguments:
 * struct mg_connection *c          Mongoose connection structure.
 * char                 *remoteIP   IP of node that made this request to us.
 * char                 *json       Inbound request.
 *
 * HTTP Response:
 * 200 if all goes well.
 * 401 if request cannot be authorized
 *
 * Notes:
 * TBD
 *========================================================================*/
void
devicePing( struct mg_connection *c, char *remoteIP, char *body )
{
    char *message;

    piboxLogger(LOG_INFO, "devicePing has been called.\n");

    message = cryptoDecrypt("iot", body, remoteIP);
    if ( message == NULL )
    {
        piboxLogger(LOG_INFO, "Unauthorized - Failed to decode ping request.");
        mg_http_reply(c, 401, "", "");
        return;
    }
    if ( strncmp(message, "ping", 4) != 0 )
    {
        piboxLogger(LOG_ERROR, "Unauthorized - decoded request doesn't match ping.\n");
        mg_http_reply(c, 401, "", "");
        return;
    }

    piboxLogger(LOG_INFO, "Received valid ping request.\n");
    mg_http_reply(c, 200, "", "");
}

/*========================================================================
 * Name:   deviceDelete
 * Prototype:  void deviceDelete( struct mg_connection *c, char *remoteIP, char *json )
 *
 * Description:
 * Delete a device, which just removes it's registration file.
 *
 * Input Arguments:
 * struct mg_connection *c          Mongoose connection structure.
 * char                 *remoteIP   IP of node that made this request to us.
 * char                 *json       Inbound request.
 *
 * HTTP Response:
 * 200 if all goes well.
 * 400 if message cannot be processed.
 * 401 if request cannot be authorized
 *
 * Notes:
 * Delete commands are issued via Jarvis.
 *========================================================================*/
void
deviceDelete( struct mg_connection *c, char *remoteIP, char *body )
{
    JSON_Value      *root_value;     // Data block wrapping JSON objects
    JSON_Object     *root_object;    // root JSON object
    char            *message = NULL;
    char            *uuid = NULL;
    char            *cmd = NULL;
    char            *ipaddr = NULL;
    char            *stampDir = NULL;
    char            *filename = NULL;
    struct stat     stat_buf;

    piboxLogger(LOG_INFO, "deviceDelete has been called.\n");

    /* Don't decrypt if from the local machine. */
    if ( (strcmp(remoteIP, LOCALHOST)==0) || utilsLocalIP(remoteIP) )
    {   
        /* pisensors will send this as properly formatted JSON, but as a string. */
        piboxLogger(LOG_INFO, "Request rawBody from localhost: %s\n", body);
        message = strdup(body);
    }
    else
    {   
        message = cryptoDecrypt("jarvis", body, remoteIP);
        if ( message == NULL )
        {
            piboxLogger(LOG_ERROR, "Unauthorized - Request from unrecognized client: %s\n", remoteIP);
            mg_http_reply(c, 401, "", "");
            return;
        }
        else
            piboxLogger(LOG_INFO, "Request params from jarvis: %s\n", message);
    }

    /* Verify the request contains the "delete" command. */
    root_value = json_parse_string(message);
    root_object = json_value_get_object(root_value);
    free(message);

    cmd = (char *)json_object_get_string( root_object, "command" );
    if ( (cmd == NULL) || (strcmp(cmd, "delete") != 0) )
    {
        piboxLogger(LOG_ERROR, "Bad request - request doesn't match delete.\n");
        mg_http_reply(c, 400, NULL, "");
        c->is_closing = 1;
        json_value_free(root_value);
        goto deviceDelete_exit;
    }
    piboxLogger(LOG_INFO, "Request command: %s\n", cmd);

    /* Verify the request contains the "uuid" key. */
    uuid = (char *)json_object_get_string( root_object, "uuid" );
    if ( uuid == NULL )
    {
        piboxLogger(LOG_ERROR, "Bad request - request doesn't include remote sensor id.\n");
        mg_http_reply(c, 400, "", "");
        json_value_free(root_value);
        goto deviceDelete_exit;
    }
    piboxLogger(LOG_INFO, "Request uuid: %s\n", uuid);

    /* Find IPv4 of specified device. */
    ipaddr = utilsGetIPv4(uuid);
    if ( ipaddr == NULL )
    {
        piboxLogger(LOG_ERROR, "Unauthorized - Delete request does not match any paired devices.\n");
        mg_http_reply(c, 401, "", "");
        json_value_free(root_value);
        goto deviceDelete_exit;
    }
    piboxLogger(LOG_INFO, "Request ipaddr: %s\n", ipaddr);

    /* Delete the file. */
    stampDir = initGetIotDir();
    filename = (char *)calloc(1, strlen(stampDir) + strlen(ipaddr) + 2 );
    sprintf(filename, "%s%s",  stampDir, ipaddr);
    if ( stat(filename, &stat_buf) == 0 )
    {
        piboxLogger(LOG_INFO, "Deleting to sensor file %s\n", filename);
        if ( !isCLIFlagSet(CLI_TEST) )
        {
            if ( unlink(filename) )
            {
                piboxLogger(LOG_ERROR, "Failed to delete sensor file %s: reasone = %s\n", filename, strerror(errno));
                mg_http_reply(c, 500, "", "");
            }
            else
            {
                mg_http_reply(c, 200, "", "");
            }
        }
        else
        {
            piboxLogger(LOG_INFO, "TESTMODE: Skipping delete sensor file %s.\n", filename);
            mg_http_reply(c, 200, "", "");
        }
    }
    else
    {
        piboxLogger(LOG_INFO, "No such sensor: %s\n", filename);
        mg_http_reply(c, 400, "", "");
    }
    json_value_free(root_value);

deviceDelete_exit:
    if ( filename )
        free( filename );
    if ( stampDir )
        free( stampDir );
    if ( ipaddr )
        free( ipaddr );
}
