/*******************************************************************************
 * imrest - a RESTful web server based on Mongoose.
 *
 * device.h:  Sensor device handling functions
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef DEVICE_H
#define DEVICE_H

/*========================================================================
 * Defined values
 *=======================================================================*/

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef DEVICE_C
extern void deviceSaveSensorData( char *json, char *ipaddr );
extern void deviceUpdate( struct mg_connection *c, char *remoteIP, char *json );
extern void deviceGetDevices( struct mg_connection *c, char *remoteIP );
extern void deviceListDevices( struct mg_connection *c, char *remoteIP, char *body );
extern void devicePing( struct mg_connection *c, char *remoteIP, char *body );
extern void deviceDelete( struct mg_connection *c, char *remoteIP, char *body );
#endif /* !DEVICE_C */
#endif /* !DEVICE_H */
