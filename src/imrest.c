/*******************************************************************************
 * imrest - a RESTful web server based on Mongoose.
 *
 * imrest.c:  program main
 *
 * Terminology
 * ------------------------------------------------------------------------
 * monitor:     Application running on RPi/PiBox used to manage IoT nodes.
 * IoT node:    Remote node typically running Arduino, or local app managing remote device.
 * Jarvis:      Remote voice control application that proxies through imrest.
 *
 * The format for REST requests is
 *
 * GET  /query/devices          From monitor            Request Device states from paired IoT nodes
 * GET  /monitor                From Jarvis             TBD
 * POST /pair/iot/<uuid>        From IoT Node           Pair an IoT node with the monitor
 * POST /pair/jarvis/<uuid>     From Jarvis             Pair a Jarvis instance with the monitor
 * POST /devices                From Jarvis             Get list of IoT nodes
 * POST /set/device             From Jarvis             Change an IoT node state
 * POST /ping                   From IoT Node           Verify IoT node registration.
 * DELETE /set/device           From Jarvis or monitor  Change an IoT node state
 *
 * Version is added to header, as such:
 * Accept-Version: 1.0
 *
 * Additionally, JSON data may be included.  This would be data
 * specific to the cmd.
 *
 * License: 0BSD
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define imrest_C

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>
#include <curl/curl.h>
#include <pibox/pibox.h>
#include <pibox/utils.h>

#include "imrest.h"

static int s_signo = 0;

static void 
signal_handler(int signo)
{
    s_signo = signo;
}

/*
 *========================================================================
 * Name:   apiVersionMatch
 * Prototype:  char *apiVersionMatch( char *apiVersion )
 *
 * Description:
 * Tests if the specified version matches what we support.
 *
 * Returns:
 * 0 if the API version is supported.
 * 1 if the API version is not supported.
 *========================================================================
 */
static int
apiVersionMatch( char *apiVersion )
{
    return (strcmp(apiVersion, VERSTR) == 0)? 0 : 1;
}

/*
 *========================================================================
 * Name:   setDefaultHeaders
 * Prototype:  char *setDefaultHeaders( void )
 *
 * Description:
 * Set common headers for outbound messages.
 *
 * Notes:
 * Uses:
 * mg_http_reply(struct mg_connection *c, int status_code, const char *headers, const char *body_fmt, ...);
 * Where headers is a collection of "name: value\n\r" strings.
 * So this function just builds that string and returns it to the caller.
 *
 * Caller is responsible for freeing the returned string.
 *========================================================================
 */
static char *
setDefaultHeaders( void )
{
    char *buf;

    // Set the Accept-Version header.
    buf = calloc(1,strlen("Accept-Version: ")+strlen(VERSTR)+3);
    sprintf(buf, "Accept-Version: %s\r\n", VERSTR);
    return(buf);
}

/*
 *========================================================================
 * Name:   handleGet
 * Prototype:  void *handleGet( struct mg_connection *c, struct mg_http_message *hm )
 *
 * Description:
 * Front end to handling GET requests.
 *========================================================================
 */
static void
handleGet( struct mg_connection *c, struct mg_http_message *hm )
{
    char    *remoteIP;

    remoteIP = utilsGetRemoteIP( c );
    piboxLogger(LOG_INFO, "Requesting IP: %s\n", remoteIP);
    if (mg_http_match_uri(hm, "/query/devices"))
    {
        deviceGetDevices( c, remoteIP );
    }
    else if (mg_http_match_uri(hm, "/monitor"))
    {
        monitorGet( c, remoteIP );
    }
    else
    {
        /* All other requests are unauthorized. */
        mg_http_reply(c, 401, NULL, "\n");
    }
    free(remoteIP);
}

/*
 *========================================================================
 * Name:   handlePost
 * Prototype:  void *handlePost( struct mg_http_message *hm )
 *
 * Description:
 * Front end to handling POST requests.
 *========================================================================
 */
static void
handlePost( struct mg_connection *c, struct mg_http_message *hm )
{
    char    *uri;
    char    *body;
    char    *remoteIP;

    uri = (char *)calloc(1, hm->uri.len + 1 );
    sprintf(uri, "%.*s", (int)hm->uri.len, hm->uri.ptr);
    piboxLogger(LOG_INFO, "uri: %s\n", uri);

    body = (char *)calloc(1, hm->body.len + 1 );
    sprintf(body, "%.*s", (int)hm->body.len, hm->body.ptr);
    piboxLogger(LOG_INFO, "body: %s\n", body);

    remoteIP = utilsGetRemoteIP( c );

    if (mg_http_match_uri(hm, "/devices"))
    {
        deviceListDevices( c, remoteIP, body );
    }
    else if (mg_http_match_uri(hm, "/set/device"))
    {
        deviceUpdate( c, remoteIP, body );
    }
    else if (mg_http_match_uri(hm, "/ping"))
    {
        devicePing( c, remoteIP, body );
    }
    else if (mg_http_match_uri(hm, PAIR_IOT))
    {
        piboxLogger(LOG_INFO, "Match PAIR_IOT: %s\n", PAIR_IOT);
        devicePairIot( c, remoteIP, body );
    }
    else if (mg_http_match_uri(hm, PAIR_JARVIS))
    {
        piboxLogger(LOG_INFO, "Match PAIR_JARVIS: %s\n", PAIR_JARVIS);
        devicePairJarvis( c, remoteIP, body );
    }
    else
    {
        /* All other requests are unauthorized. */
        mg_http_reply(c, 401, NULL, "\n");
    }

    free(remoteIP);
    free(body);
    free(uri);
}

/*
 *========================================================================
 * Name:   handleDelete
 * Prototype:  void *handleDelete( struct mg_http_message *hm )
 *
 * Description:
 * Front end to handling DELETE requests.
 *========================================================================
 */
static void
handleDelete( struct mg_connection *c, struct mg_http_message *hm )
{
    char    *body;
    char    *remoteIP;

    body = (char *)calloc(1, hm->body.len + 1 );
    sprintf(body, "%.*s", (int)hm->body.len, hm->body.ptr);

    remoteIP = utilsGetRemoteIP( c );
    if (mg_http_match_uri(hm, "/set/device"))
    {
        deviceDelete( c, remoteIP, body );
    }
    else
    {
        mg_http_reply(c, 401, NULL, "\n");
    }

    free(remoteIP);
    free(body);
}

/*
 *========================================================================
 * Name:   eventHandler
 * Prototype:  void eventHandler( void )
 *
 * Description:
 * Handler called by mg_mgr_poll() when inbound messages arrive.
 * This just tests the HTTP method and passes the messages to the appropriate
 * method handlers.
 *
 * Notes:
 * Every inbound HTTP message contains a JSON object with two fields.
 * iv field       - The initializing vector used with AES 128 CBC encryption.
 * message field  - A JSON command object that is AES 128 CBC encypted using the 
 *                  IV in the HTTP message and the UUID of the sending node.
 *
 * The contents of the JSON command object in the message field depends on the 
 * imrest API being processed.  Fields in this JSON can include a UUID of
 * a node, the command to send to the node and a new state for the node.
 *========================================================================
 */
static void
frontEnd(struct mg_connection *c, int ev, void *ev_data)
{
    char    *method;

    if (ev == MG_EV_HTTP_MSG) 
    {
        // The MG_EV_HTTP_MSG event means HTTP request. `hm` holds parsed request,
        // see https://mongoose.ws/documentation/#struct-mg_http_message
        struct mg_http_message *hm = (struct mg_http_message *) ev_data;

        method = (char *)calloc(1, hm->method.len + 1 );
        sprintf(method, "%.*s", (int)hm->method.len, hm->method.ptr);

        piboxLogger(LOG_INFO, "Inbound method: %s\n", method);
        if ( strcmp(method, "GET") == 0 )
            handleGet(c, hm);
        else if ( strcmp(method, "POST") == 0 )
            handlePost(c, hm);
        else if ( strcmp(method, "DELETE") == 0 )
            handleDelete(c, hm);
        else 
        {
            piboxLogger(LOG_ERROR, "Unsupported HTTP method: %s\n", method);
            mg_http_reply(c, 401, NULL, "\n");
        }

        free(method);
    }
}

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * Program startup
 *
 * Notes:
 * This is a RESTful web server based on Mongoose.
 * It's purpose is to provide a web inteface for sensors and Jarvis to
 * communication with Ironman's monitor.
 * ========================================================================
 */
int
main(int argc, char *argv[])
{
    struct mg_mgr   mgr;
    char            server_url[128];

    /* Load saved configuration and parse command line */
    initConfig();
    parseArgs(argc, argv);

    /* Setup logging */
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);
    printf("Verbosity level: %d\n", piboxLoggerGetVerbosity());

    if ( cliOptions.logFile != NULL )
    {
        piboxLogger(LOG_INFO, "Log file: %s\n", cliOptions.logFile);
    }
    else
    {
        piboxLogger(LOG_INFO, "No log file configured.\n");
    }

    /* Make sure configured options are okay. */
    if ( validateConfig() != 0 )
    {
        piboxLogger(LOG_ERROR, "Configuration failure.\n");
        goto skipit;
    }

    /* Setup signal handling */
    signal(SIGINT, signal_handler);
    signal(SIGTERM, signal_handler);

    /* Initialize server variables. */
    initSetup();

    /* Initialize libcurl. */
    curl_global_init(CURL_GLOBAL_ALL);

    /* Initialize Mongoose. */
    mg_mgr_init(&mgr);        // Init manager
    mg_log_set(MG_LL_INFO);  // Set debug log level. Default is MG_LL_INFO
    sprintf(server_url, "http://0.0.0.0:%d", HTTP_PORT);
    mg_http_listen(&mgr, server_url, frontEnd, NULL);  // Setup listener

    /* Spin waiting for connections. */
    while ( s_signo == 0 )
        mg_mgr_poll(&mgr, 1000);  

    /* Clean up Mongoose. */
    mg_mgr_free(&mgr);

skipit:
    piboxLoggerShutdown();
    return 0;
}
