/*******************************************************************************
 * imrest - a RESTful web server based on Mongoose.
 *
 * imrest.h:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef IMREST_H
#define IMREST_H

/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef IMREST_C
#endif /* IMREST_C */

/*========================================================================
 * Defined values
 *=======================================================================*/
#define PROG        "imrest"
#define MAXBUF      4096
#define HTTP_PORT   8165
#define HTTP_PORT_T 8166

// Where the config file is located
#define F_CFG   "/etc/imrest.cfg"
#define F_CFG_T "data/imrest.cfg"

#define PAIR_IOT     "/pair/iot"
#define PAIR_JARVIS  "/pair/jarvis"

/* Enable AES 128 CBC encryption */
#define CBC 1

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef IMREST_C
#endif

/*========================================================================
 * Include other headers
 *=======================================================================*/
#include <pibox/log.h>
#include <pibox/parson.h>
#include "mongoose.h"
#include "cli.h"
#include "utils.h"
#include "msgQueue.h"
#include "init.h"
#include "base64.h"
#include "aes.h"
#include "pkcs7_padding.h"
#include "crypto.h"
#include "device.h"
#include "monitor.h"
#include "pair.h"

#endif /* !IMREST_H */
