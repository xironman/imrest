/*******************************************************************************
 * imrest - a RESTful web server based on Mongoose.
 *
 * init.c:  Various utility methods
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define INIT_C

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <uuid.h>
#include <sys/types.h>

#include "imrest.h"

/* Local Prototypes */
char *initGetUUID( void );

static struct __imrest_init {
    uuid_t uuid;
    char *pairEnabledStamp;
    char *ironmanDir;
    char *monitorDir;
    char *stampDir;
    char *iotDir;
    char *descriptorFile;
} imrest_init;

/*========================================================================
 * Name:   initSetup
 * Prototype:  void initSetup( void )
 *
 * Description:
 * Builds directory and filenames based on runtime configuration.
 *
 * Input Arguments:
 * N/A
 *
 * Returns:
 * N/A
 *
 * Notes:
 * Memory allocated here will persist through the life of the program.
 *========================================================================*/
void
initSetup( void )
{
    char *uuidStr;

    piboxLogger(LOG_INFO, "init.setup has been called.\n");
    uuid_generate(imrest_init.uuid);

    /* Set directory and stamp files based on where the webroot has been configured. */
    imrest_init.pairEnabledStamp = (char *)calloc(1,strlen(cliOptions.webroot) + strlen(PAIRENABLEDSTAMP) + 2);
    imrest_init.ironmanDir       = (char *)calloc(1,strlen(cliOptions.webroot) + strlen(IRONMANDIR) + 2);
    imrest_init.monitorDir       = (char *)calloc(1,strlen(cliOptions.webroot) + strlen(MONITORDIR) + 2);

    sprintf(imrest_init.pairEnabledStamp, "%s%s", cliOptions.webroot, PAIRENABLEDSTAMP);
    sprintf(imrest_init.ironmanDir, "%s%s", cliOptions.webroot, IRONMANDIR);
    sprintf(imrest_init.monitorDir, "%s%s", cliOptions.webroot, MONITORDIR);

    imrest_init.stampDir = (char *)calloc(1,strlen(imrest_init.ironmanDir) + strlen(STAMPDIR) + 2);
    imrest_init.iotDir   = (char *)calloc(1,strlen(imrest_init.ironmanDir) + strlen(IOTDIR) + 2);

    sprintf(imrest_init.stampDir, "%s%s", imrest_init.ironmanDir, STAMPDIR);
    sprintf(imrest_init.iotDir, "%s%s", imrest_init.ironmanDir, IOTDIR);

    imrest_init.descriptorFile = (char *)calloc(1,strlen(imrest_init.monitorDir) + strlen(DESCRIPTORFILE) + 2);
    sprintf(imrest_init.descriptorFile, "%s%s", imrest_init.monitorDir, DESCRIPTORFILE);

    piboxLogger(LOG_INFO, "Webroot directory : %s\n", cliOptions.webroot);
    piboxLogger(LOG_INFO, "Pair Enabled stamp: %s\n", imrest_init.pairEnabledStamp);
    piboxLogger(LOG_INFO, "Ironman directory : %s\n", imrest_init.ironmanDir);
    piboxLogger(LOG_INFO, "Monitor directory : %s\n", imrest_init.monitorDir);
    piboxLogger(LOG_INFO, "Stamp directory   : %s\n", imrest_init.stampDir);
    piboxLogger(LOG_INFO, "IoT directory     : %s\n", imrest_init.iotDir);
    piboxLogger(LOG_INFO, "Descriptor file   : %s\n", imrest_init.descriptorFile);
    uuidStr = initGetUUID();
    piboxLogger(LOG_INFO, "Monitor UUID      : %s\n", uuidStr);
    free(uuidStr);
}

/*========================================================================
 * Name:   initGetPairEnabled
 * Prototype:  int initGetPairEnabled( void )
 *
 * Description:
 * Test if pairing is enabled on the Ironman monitor.
 *
 * Input Arguments:
 * N/A
 *
 * Returns:
 * 0 if pairing is not enabled.
 * 1 if pairing is enabled.
 *
 * Notes:
 * The monitor UI will create a stamp file when pairing is enabled.
 * If that file is found, return 1.  Otherwise, return 0.
 *========================================================================*/
int
initGetPairEnabled( void )
{
    struct stat     stat_buf;

    if ( stat( imrest_init.pairEnabledStamp, &stat_buf) == 0 )
        return 1;
    else
        return 0;
}

/*========================================================================
 * Name:   initGetDataDir
 * Prototype:  char *initGetDataDir( void )
 *
 * Description:
 * Retrieve the webroot directory.
 *
 * Input Arguments:
 * N/A
 *
 * Returns:
 * N/A
 *
 * Notes:
 * This just wraps cliOptions.webroot, which is program global anyway.
 * Caller should NOT attempt to free returned pointer.
 *========================================================================*/
char *
initGetDataDir( void )
{
    return(cliOptions.webroot);
}

/*========================================================================
 * Name:   initGetIronmanDir
 * Prototype:  char *initGetIronmanDir( void )
 *
 * Description:
 * Returns a copy of the ironman directory string.  This is the directory used
 * to hold TBD files.
 *
 * Input Arguments:
 * N/A
 *
 * Returns:
 * Character string.  Caller is responsible for freeing the returned pointer.
 *
 * Notes:
 * N/A
 *========================================================================*/
char *
initGetIronmanDir( void )
{
    return( strdup(imrest_init.ironmanDir) );
}

/*========================================================================
 * Name:   initGetStampDir
 * Prototype:  char *initGetStampDir( void )
 *
 * Description:
 * Returns a copy of the stamp directory string.  This is the directory used
 * to hold TBD files.
 *
 * Input Arguments:
 * N/A
 *
 * Returns:
 * Character string.  Caller is responsible for freeing the returned pointer.
 *
 * Notes:
 * N/A
 *========================================================================*/
char *
initGetStampDir( void )
{
    return( strdup(imrest_init.stampDir) );
}

/*========================================================================
 * Name:   initGetIotDir
 * Prototype:  char *initGetIotDir( void )
 *
 * Description:
 * Returns a copy of the IoT directory string.  This is the directory used
 * to hold TBD files.
 *
 * Input Arguments:
 * N/A
 *
 * Returns:
 * Character string.  Caller is responsible for freeing the returned pointer.
 *
 * Notes:
 * N/A
 *========================================================================*/
char *
initGetIotDir( void )
{
    return ( strdup(imrest_init.iotDir) );
}

/*========================================================================
 * Name:   initGetDescriptorFile
 * Prototype:  char *initGetDescriptorFile( void )
 *
 * Description:
 * Returns a copy of the descriptor file string.  This is the file
 * that describes, in human terms, an ironman instance.
 *
 * Input Arguments:
 * N/A
 *
 * Returns:
 * Character string.  Caller is responsible for freeing the returned pointer.
 *
 * Notes:
 * N/A
 *========================================================================*/
char *
initGetDescriptorFile( void )
{
    return ( strdup(imrest_init.descriptorFile) );
}

/*========================================================================
 * Name:   initGetUUID
 * Prototype:  char *initGetUUID( void )
 *
 * Description:
 * Returns the application UUID, which is generated at program start.
 *
 * Input Arguments:
 * None.
 *
 * Returns:
 * Buffer holding 36 character uuid (plus null terminator).  Caller is responsible
 * for freeing the buffer.
 *
 * Notes:
 * Does not test if uuid has been initialized.
 *========================================================================*/
char *
initGetUUID( void )
{
    char    *value;
    value = (char *)calloc(1,37);
    uuid_unparse(imrest_init.uuid, value);
    return (value);
}
