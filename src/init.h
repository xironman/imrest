/*******************************************************************************
 * imrest - a RESTful web server based on Mongoose.
 *
 * init.h:  Web service initialization
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef INIT_H
#define INIT_H

/*========================================================================
 * Defined values
 *=======================================================================*/

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/

#define DATADIR             "data"
#define PAIRENABLEDSTAMP    "/imnetconfig"
#define IRONMANDIR          "/ironman"
#define MONITORDIR          "/monitor"
#define STAMPDIR            "/jarvis/"
#define IOTDIR              "/iot/"
#define DESCRIPTORFILE      "/descriptor"

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef INIT_C
extern void initSetup( void );
extern void initSetDataDir( char *source );
extern int  initGetPairEnabled( void );
extern char *initGetDataDir( void );
extern char *initGetIronmanDir( void );
extern char *initGetStampDir( void );
extern char *initGetIotDir( void );
extern char *initGetDescriptorFile( void );
extern char *initGetUUID( void );
#endif /* !INIT_C */
#endif /* !INIT_H */
