/*******************************************************************************
 * imrest - a RESTful web server based on Mongoose.
 *
 * monitor.c:  monitor handling functions
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define DEVICE_C

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <uuid.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "imrest.h"

/*========================================================================
 *========================================================================
 * STATIC FUNCTIONS
 *========================================================================
 *========================================================================*/

/*========================================================================
 *========================================================================
 * PUBLIC FUNCTIONS
 *========================================================================
 *========================================================================*/

/*========================================================================
 * Name:   monitorGet
 * Prototype:  void monitorGet( struct mg_connection *c, char *remoteIP )
 *
 * Description:
 * Get the monitor's description file.
 *
 * Input Arguments:
 * struct mg_connection *c          Mongoose connection structure.
 * char                 *remoteIP   IP of node that made this request to us.
 *
 * HTTP Response:
 * 200 if all goes well.
 * 400 if message cannot be processed.
 * 401 if request cannot be authorized
 *
 * Notes:
 * TBD
 *========================================================================*/
void
monitorGet( struct mg_connection *c, char *remoteIP )
{
    char            *json = NULL;
    char            *monitorFile = NULL;
    char            *jarvis_uuid = NULL;
    char            *crypt_json = NULL;
    char            *buf = NULL;
    JSON_Value      *root_value;
    JSON_Object     *root_object;
    int             rc = 200;
    struct stat     stat_buf;
    FILE            *fd;

    piboxLogger(LOG_INFO, "Entered.\n");

    /* Get the Jarvis UUID, if registered. */
    jarvis_uuid = utilsLoadUUID("jarvis", remoteIP);
    if ( jarvis_uuid == NULL )
    {
        piboxLogger(LOG_ERROR, "Request from unrecognized client: %s\n", remoteIP);
        rc = 401;
        goto monitorGet_exit;
    }

    /* Get the descriptor filename. */
    monitorFile = initGetDescriptorFile();
    piboxLogger(LOG_INFO, "Monitor file: %s\n", monitorFile);

    /* Get file size. */
    stat(monitorFile, &stat_buf);
    if ( stat_buf.st_size == 0 )
    {
        piboxLogger(LOG_ERROR, "Empty descriptor file.\n");
        rc = 401;
        goto monitorGet_exit;
    }

    /* Read the file */
    fd = fopen(monitorFile, "r");
    if ( !fd )
    {
        piboxLogger(LOG_ERROR, "Can't open descriptor file.\n");
        rc = 401;
        goto monitorGet_exit;
    }
    buf = (char *)calloc(1, stat_buf.st_size + 2);
    if ( fread(buf, 1, stat_buf.st_size, fd) != stat_buf.st_size )
    {
        piboxLogger(LOG_ERROR, "Failed read of descriptor file.\n");
        free(buf);
        rc = 401;
        goto monitorGet_exit;
    }

    root_value = json_value_init_object();
    root_object = json_value_get_object(root_value);
    json_object_set_string( root_object, "descriptor", buf );
    json = json_serialize_to_string(root_value);
    free(buf);

    /* Encrypt the response */
    crypt_json = cryptoEncrypt(jarvis_uuid, json);
    if ( crypt_json == NULL )
        rc = 400;
    else
        piboxLogger(LOG_INFO, "Response has been encrypted: %s\n", crypt_json);
    json_free_serialized_string(json);
    json_value_free(root_value);

monitorGet_exit:
    if ( crypt_json )
        mg_http_reply(c, rc, "", "%s\n", crypt_json);
    else
        mg_http_reply(c, rc, "", "\n");

    /* Clean up */
    if ( crypt_json != NULL )
        free(crypt_json);
    if ( jarvis_uuid != NULL )
        free(jarvis_uuid);
    if ( monitorFile != NULL )
        free(monitorFile);
}
