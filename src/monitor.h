/*******************************************************************************
 * imrest - a RESTful web server based on Mongoose.
 *
 * monitor.h:  monitor handling functions
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef MONITOR_H
#define MONITOR_H

/*========================================================================
 * Defined values
 *=======================================================================*/

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef MONITOR_C
extern void monitorGet( struct mg_connection *c, char *remoteIP );
#endif /* !MONITOR_C */
#endif /* !MONITOR_H */
