/*******************************************************************************
 * imrest - a RESTful web server based on Mongoose.
 *
 * msgQueue.c:  Message Queue Management
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define MSGQUEUE_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>

#include "imrest.h"

// The head of the message queues.
IMREST_MSG_T *inQueue = NULL;    // Inbound messages, yet to be processed.

/*
 *========================================================================
 *========================================================================
 *
 * Queue Management
 *
 *========================================================================
 *========================================================================
 */

/*========================================================================
 * Name:   queueSize
 * Prototype:  int queueSize( void )
 *
 * Description:
 * Retrieve the current size of the queue.
 *
 * Arguments:
 * N/A
 *
 * Returns:
 * The size of the queue.
 *========================================================================*/
int
queueSize( void )
{
    int             count = 0;
    IMREST_MSG_T     *ptr  = NULL;

    pthread_mutex_lock( &queueMutex );
    ptr  = inQueue;
    while ( ptr!=NULL )
    {
        count++;
        ptr = ptr->next;
    }
    pthread_mutex_unlock( &queueMutex );
    return count;
}

/*========================================================================
 * Name:   freeMsgNode
 * Prototype:  void freeMsgNode( IMREST_MSG_T * )
 *
 * Description:
 * Free a message queue node.  It should already have been popped from its queue.
 *
 * Input Arguments:
 * IMREST_MSG_T *node     Node to free.
 *========================================================================*/
void
freeMsgNode( IMREST_MSG_T *node )
{
    if ( node == NULL )
        return;
    if ( node->response != NULL )
        free(node->response);
    if ( node->id != NULL )
        free(node->id);
    free(node);
}

/*========================================================================
 * Name:   popMsgQueue
 * Prototype:  IMREST_MSG_T *popMsgQueue( void )
 *
 * Description:
 * Pull the next message from the inbound queue, if possible.
 *
 * Returns
 * Pointer to a node if found, NULL otherwise.  If found, caller is responsible
 * for freeing the node using freeMsgNode().
 *========================================================================*/
IMREST_MSG_T *
popMsgQueue( void )
{
    IMREST_MSG_T    *ptr  = NULL;
    IMREST_MSG_T    *next = NULL;

    pthread_mutex_lock( &queueMutex );
    ptr = inQueue;
    if ( ptr != NULL )
        next = ptr->next;
    inQueue = next;
    pthread_mutex_unlock( &queueMutex );
    return ( ptr );
}

/*========================================================================
 * Name:   queueMsg
 * Prototype:  void queueMsg( char *, char * )
 *
 * Description:
 * Queue a message for processing.
 * The new message goes on the end of the list.
 *
 * Input Arguments:
 * char *response        The response from a sensor after a command is sent.
 * char *id              Unique identifier for this response.
 *========================================================================*/
void 
queueMsg( char *id )
{
    IMREST_MSG_T    *ptr  = NULL;
    IMREST_MSG_T    *last = NULL;

    pthread_mutex_lock( &queueMutex );
    ptr = inQueue;
    while (ptr != NULL)
    {
        last = ptr;
        ptr = ptr->next;
    }
    ptr = (IMREST_MSG_T *)malloc(sizeof(IMREST_MSG_T));
    ptr->next = NULL;
    ptr->response = NULL;
    ptr->id = id;

    if ( last == NULL )
        inQueue = ptr;
    else
        last->next = ptr;

    pthread_mutex_unlock( &queueMutex );
    piboxLogger(LOG_INFO, "Added to inbound queue, new size: %d\n", queueSize());

    /* Wake the processor */
    if ( sem_post(&queueSem) != 0 )
        piboxLogger(LOG_ERROR, "Failed to post queueSem semaphore.\n");
}

/*========================================================================
 * Name:   clearQueue
 * Prototype:  clearQueue( void )
 *
 * Description:
 * Clear the inbound message queue.
 *========================================================================*/
void
clearMsgQueue( void )
{
    IMREST_MSG_T *msg = NULL;
    while ( (msg=popMsgQueue()) != NULL )
        freeMsgNode(msg);
}

/*========================================================================
 * Name:   updateMsgQueue
 * Prototype:  void updateMsgQueue( char *, char * )
 *
 * Description:
 * Update the response field for the specified ID, if it exists.
 *========================================================================*/
void
updateMsgQueue( char *id, char *response )
{
    IMREST_MSG_T    *ptr  = NULL;

    pthread_mutex_lock( &queueMutex );
    ptr = inQueue;
    while (ptr != NULL)
    {
        if ( (ptr->id != NULL) && (strcmp(ptr->id, id)==0) )
        {
            ptr->response = response;
            break;
        }
        ptr = ptr->next;
    }
    pthread_mutex_unlock( &queueMutex );
}

/*========================================================================
 * Name:   getMsgQueue
 * Prototype:  void getMsgQueue( char * )
 *
 * Description:
 * Retrieve the response field for the specified ID, if it exists.
 *========================================================================*/
char *
getMsgQueue( char *id )
{
    IMREST_MSG_T    *ptr  = NULL;
    char            *response = NULL;

    pthread_mutex_lock( &queueMutex );
    ptr = inQueue;
    while (ptr != NULL)
    {
        if ( (ptr->id != NULL) && (strcmp(ptr->id, id)==0) )
        {
            response = ptr->response;
            break;
        }
        ptr = ptr->next;
    }
    pthread_mutex_unlock( &queueMutex );
    return (response);
}

