/*******************************************************************************
 * imrest - a RESTful web server based on Mongoose.
 *
 * msgQueue.h:  Message Queue Management
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef MSGQUEUE_H
#define MSGQUEUE_H

#include <semaphore.h>

/*
 * ========================================================================
 * Typedefs
 * =======================================================================
 */
typedef struct __imrest_msg_t {
    struct __imrest_msg_t   *next;
    char                    *response;
    char                    *id;
} IMREST_MSG_T;

/*
 * ========================================================================
 * Globals
 * =======================================================================
 */
#ifdef MSGQUEUE_C
pthread_mutex_t queueMutex = PTHREAD_MUTEX_INITIALIZER;
sem_t           queueSem;
#else
extern pthread_mutex_t queueMutex;
extern sem_t           queueSem;
#endif /* MSGQUEUE_C */

/*
 * ========================================================================
 * Prototypes
 * =======================================================================
 */
#ifndef MSGQUEUE_C
extern int         queueSize( int );
extern void        freeMsgNode( IMREST_MSG_T * );
extern IMREST_MSG_T *popMsgQueue( void );
extern void        queueMsg( char * );
extern void        clearMsgQueue( void );
extern void        updateMsgQueue( char *id, char *response );
extern char        *getMsgQueue( char *id );
#endif /* MSGQUEUE_C */

#endif /* !MSGQUEUE_H */
