/*******************************************************************************
 * imrest - a RESTful web server based on Mongoose.
 *
 * pair.c:  Sensor device handling functions
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define PAIR_C

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <uuid.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "imrest.h"

/*========================================================================
 *========================================================================
 * STATIC FUNCTIONS
 *========================================================================
 *========================================================================*/

/*========================================================================
 * Name:   handle_uuid
 * Prototype:  int handle_uuid( struct mg_connection *c, char *stampDir, char *ipaddr, char *json )
 *
 * Description:
 * Register a device, which creates it's registration file.
 *
 * Input Arguments:
 * struct mg_connection *c      Mongoose connection structure.
 * char *stampDir               Directory where registration file should be written.
 * char *ipaddr                 IP address of remote node to register.
 * char *json                   JSON sent by remote node for registration.
 *
 * Returns:
 * 0 if the registration succeeds.
 * 1 if the registration fails.
 *========================================================================*/
static void
handle_uuid( struct mg_connection *c, char *stampDir, char *requestIP, char *json )
{
    char            *filename;
    struct stat     stat_buf;
    FILE            *fd;

    piboxLogger(LOG_INFO, "handle_uuid has been called: stampDir: %s, json = %s\n", stampDir, json);

    /* Make sure destintation directory exists. */
    if ( stat(stampDir, &stat_buf) != 0 )
    {
        mkdir(stampDir, 0755);
    }

    /* Get name of file to write to. */
    filename = (char *)calloc(1, strlen(stampDir) + strlen(requestIP) + 2);
    sprintf(filename, "%s/%s", stampDir, requestIP);
    piboxLogger(LOG_INFO, "Registration file: %s\n", filename);

    /* We're registering, so delete any existing registration. */
    unlink(filename);

    /* Open file for writing. */
    fd = fopen(filename, "w");
    if ( fd == NULL )
    {
        piboxLogger(LOG_ERROR, "Failed to open registration file.\n");
        mg_http_reply(c, 401, "", "");
        free(filename);
        return;
    }

    /* Write JSON from remote node to registration file. */
    if ( fwrite(json, 1, strlen(json), fd) != strlen(json) )
    {
        piboxLogger(LOG_ERROR, "Failed write to registration file.\n");
        mg_http_reply(c, 401, "", "");
    }
    else
    {
        piboxLogger(LOG_INFO, "Wrote registration stamp file: %s\n", filename);
        mg_http_reply(c, 200, "", "");
    }
    free(filename);
    fclose(fd);
}

/*========================================================================
 *========================================================================
 * PUBLIC FUNCTIONS
 *========================================================================
 *========================================================================*/

/*========================================================================
 * Name:   devicePairIot
 * Prototype:  void devicePairIot( struct mg_connection *c, char *remoteIP, char *json )
 *
 * Description:
 * Delete a device, which just removes it's registration file.
 *
 * Input Arguments:
 * struct mg_connection *c          Mongoose connection structure.
 * char                 *remoteIP   IP of node that made this request to us.
 * char                 *json       Inbound request.
 *
 * HTTP Response:
 * 200 if all goes well.
 * 401 if request cannot be authorized
 *========================================================================*/
void
devicePairIot( struct mg_connection *c, char *remoteIP, char *body )
{
    char *stampDir;

    piboxLogger(LOG_INFO, "Entered.\n");

    /* Only allow pairing if server is in registration mode. */
    if ( !initGetPairEnabled() )
    {
        piboxLogger(LOG_INFO, "Not in registration mode - ignoring IoT pair request from %s\n", remoteIP);
        mg_http_reply(c, 401, "", "");
        return;
    }

    /*
     * Handle an IoT device
     */
    stampDir = initGetIotDir();
    handle_uuid(c, stampDir, remoteIP , body);
    free(stampDir);
}

/*========================================================================
 * Name:   devicePairJarvis
 * Prototype:  void devicePairJarvis( struct mg_connection *c, char *remoteIP, char *json )
 *
 * Description:
 * Delete a device, which just removes it's registration file.
 *
 * Input Arguments:
 * struct mg_connection *c          Mongoose connection structure.
 * char                 *remoteIP   IP of node that made this request to us.
 * char                 *json       Inbound request.
 *
 * HTTP Response:
 * 200 if all goes well.
 * 400 if message cannot be processed.
 * 401 if request cannot be authorized
 *
 * Notes:
 * Delete commands are issued via Jarvis.
 *========================================================================*/
void
devicePairJarvis( struct mg_connection *c, char *remoteIP, char *body )
{
    char *stampDir;

    piboxLogger(LOG_INFO, "Entered.\n");

    /* Only allow pairing if server is in registration mode. */
    if ( !initGetPairEnabled() )
    {
        piboxLogger(LOG_INFO, "Not in registration mode - ignoring IoT pair request from %s\n", remoteIP);
        mg_http_reply(c, 401, "", "");
        return;
    }

    /*
     * Handle an Jarvis device
     */
    stampDir = initGetStampDir();
    handle_uuid(c, stampDir, remoteIP , body);
    free(stampDir);
}

