/*******************************************************************************
 * imrest - a RESTful web server based on Mongoose.
 *
 * pair.h:  Sensor pair handling functions
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef PAIR_H
#define PAIR_H

/*========================================================================
 * Defined values
 *=======================================================================*/

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef PAIR_C
extern void devicePairIot( struct mg_connection *c, char *remoteIP, char *body );
extern void devicePairJarvis( struct mg_connection *c, char *remoteIP, char *body );
#endif /* !PAIR_C */
#endif /* !PAIR_H */
