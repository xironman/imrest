/*******************************************************************************
 * imrest - a RESTful web server based on Mongoose.
 *
 * utils.c:  Various utility methods
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define UTILS_C

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <unistd.h>
#include <uuid.h>
#include <arpa/inet.h>
#include <curl/curl.h>
#include <pibox/pibox.h>

#include "imrest.h"

struct response {
    char    *response;
};

/* Local prototypes. */
char *utilsGenUUID( void );

/* piboxd listens on this port */
#define PIBOXD_PORT 13910

/*========================================================================
 * Name:   parse
 * Prototype:  int utilsParse( char *, char **, int )
 *
 * Description:
 * Parse a character string into an array of character tokens.
 *
 * Input Arguments:
 * char *line       The string to parse
 * char **argv      The parsed tokens
 * int  max         Maximum number of arguments to parse
 *
 * Returns:
 * Number of arguments in argv.
 *
 * Note:
 * Borrowed from http://www.csl.mtu.edu/cs4411.ck/www/NOTES/process/fork/shell.c
 *========================================================================*/
int
utilsParse(char *line, char **argv, int max)
{
    int idx = 0;

    /* if not the end of line ....... */
    while (*line != '\0')
    {
        /* replace white spaces with 0 */
        while (*line == ' ' || *line == '\t' || *line == '\n')
            *line++ = '\0';

        /* save the argument position */
        if ( idx < max )
            *argv++ = line;

        /* skip the argument until ... */
        while (*line != '\0' && *line != ' ' && *line != '\t' && *line != '\n')
            line++;

        if ( ++idx == max )
            break;
    }

    /* mark the end of argument list  */
    *argv = '\0';

    return idx;
}

/*========================================================================
 * Name:   utilsLoadUUID
 * Prototype:  char *utilsLoadUUID( char *, char * )
 *
 * Description:
 * Load a devices UUID from it's stored file.
 *
 * Input Arguments:
 * char *source       One of "jarvis" or "iot"
 * char *requestIP    The IP address of the device or Jarvis node.
 *
 * Returns:
 * A UUID string or NULL if the string can't be retrieved.
 * The caller is required to free the returned string.
 *========================================================================*/
char *
utilsLoadUUID(char *source, char *requestIP)
{
    char            *srcDir;
    char            *filename;
    char            *value;
    char            *uuid = NULL;
    char            *data;
    struct stat     stat_buf;
    FILE            *fd;
    JSON_Value      *root_value;
    JSON_Object     *root_object;

    if ( strcmp(source,"jarvis") == 0 )
    {   
        srcDir = initGetStampDir();
    }
    else
    {   
        srcDir = initGetIotDir();
    }
    if ( srcDir == NULL )
    {
        piboxLogger(LOG_ERROR, "Can't get srcDir.\n");
        return NULL;
    }
    piboxLogger(LOG_INFO, "iotDir: %s\n", srcDir);
    filename = (char *)calloc(1, strlen(srcDir) + strlen(requestIP) + 2);
    sprintf(filename, "%s%s", srcDir, requestIP);
    piboxLogger(LOG_INFO, "%s file: %s\n", source, filename);
    free(srcDir);

    if ( stat(filename, &stat_buf) == 0 )
    {   
        fd = fopen(filename, "r");
        if ( fd )
        {
            data = (char *)calloc(1, stat_buf.st_size + 1);
            if ( fread(data, 1, stat_buf.st_size, fd) == stat_buf.st_size )
            {
                piboxLogger(LOG_INFO, "File data: %s\n", data);
                root_value = json_parse_string(data);
                root_object = json_value_get_object(root_value);
                value = (char *)json_object_get_string( root_object, "uuid" );
                if ( value )
                    uuid = strdup(value);
                json_value_free(root_value);
            }
            else
                piboxLogger(LOG_ERROR, "Failed to read sensor file: %s\n", filename);
            free(data);
            fclose(fd);
        }
        else
            piboxLogger(LOG_ERROR, "Failed to open sensor file: %s\n", filename);
    }
    else
        piboxLogger(LOG_ERROR, "Failed to find sensor file: %s\n", filename);

    free(filename);
    return uuid;
}

/*========================================================================
 * Name:   utilsLocalIP
 * Prototype:  int utilsLocalIP( char * )
 *
 * Description:
 * Test if an IP is from the local host (re: loopback or assigned IP).
 *
 * Input Arguments:
 * char *requestIP    The IP address of the remote device to check.
 *
 * Returns:
 * 0 if the requestIP is not from the local host.
 * 1 if the requestIP is from the local host.
 *========================================================================*/
int
utilsLocalIP(char *requestIP)
{
    int fd;
    int i=0;
    int rc=0;
    struct ifreq ifr;
    char ipaddr[32];
    DIR *pdir = NULL;
    struct dirent *pent = NULL;

    piboxLogger(LOG_INFO, "Checking %s\n", requestIP);
    if ( strcmp(requestIP, LOCALHOST) == 0 )
        return 1;

    pdir = opendir( "/sys/class/net" );
    if (pdir != NULL)
    {
        /* Iterate files */
        while( (pent = readdir(pdir)) != NULL )
        {
            /* Get a socket to use with the ioctl. */
            fd = socket(AF_INET, SOCK_DGRAM, 0);
            memset(&ifr, 0, sizeof(ifr));
            ifr.ifr_addr.sa_family = AF_INET;

            /* Copy the interface name in the ifreq structure */
            memcpy(ifr.ifr_name , pent->d_name , IFNAMSIZ-1);

            /* Issue ioctl to get IP addresses. */
            if ( ioctl(fd, SIOCGIFADDR, &ifr) != 0 )
            {
                continue;
            }

            /* Close the socket. */
            close(fd);

            memset(ipaddr, 0 , 32);
            strcpy(ipaddr, inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr) );
            if ( strcmp(requestIP, ipaddr) == 0 )
            {
                rc=1;
                break;
            }
            i++;
        }
        closedir(pdir);
    }
    return rc;
}

/*========================================================================
 * Name:   utilsGetDescripor
 * Prototype:  char *utilsGetDescriptor( void )
 *
 * Description:
 * Read the descriptor file.
 *
 * Input Arguments:
 * N/A
 *
 * Returns:
 * String of the contents of the descriptor file or a default string
 * if the descriptor can't be read.
 *========================================================================*/
char *
utilsGetDescriptor( void )
{
    char        *filename;
    char        *buf = NULL;
    struct stat stat_buf;
    FILE        *fd = NULL;

    filename = initGetDescriptorFile();

    /* Allocate buffer for file contents */
    if ( stat(filename, &stat_buf) == 0 )
    {
        fd = fopen(filename, "r");
        if ( fd )
            buf = (char *)calloc(1, stat_buf.st_size + 1);
    }
    if ( fd == NULL )
    {
        buf = (char *)calloc(1, strlen(DEFAULT_DESCRIPTOR) + 1);
        sprintf(buf, "%s", DEFAULT_DESCRIPTOR);
    }
    else
    {
        /* Load file contents into buffer */
        fgets(buf, stat_buf.st_size, fd);
        fclose(fd);
    }

    free(filename);
    return buf;
}

/*========================================================================
 * Name:   utilsGetIPv4
 * Prototype:  char *utilsGetIPv4( char *uuid )
 *
 * Description:
 * Search all IoT configurations for a matching uuid.  If found, return the IPv4 address,
 * which is just the filename.  If not found return null.
 *
 * Input Arguments:
 * char *uuid       UUID to search for
 *
 * Returns:
 * Character string holding the IP address of the remote device.
 * Caller is responsible for freeing the returned pointer.
 *========================================================================*/
char *
utilsGetIPv4( char *uuid )
{
    DIR *pdir = NULL;
    struct dirent *pent = NULL;
    char *dirname;
    char *data;
    char *filename = NULL;

    /* Open iotDir */
    dirname = initGetIotDir();
    piboxLogger(LOG_INFO, "iotDir: %s\n", dirname);
    pdir = opendir( dirname );
    if (pdir != NULL)
    {
        /* Iterate files */
        while( (pent = readdir(pdir)) != NULL )
        {   
            /* Skip non-sensor-device entries */
            if ( strncmp(pent->d_name, ".", 1) == 0 )
                continue;

            /* Call utilsLoadUUID() to read UUID from file */
            piboxLogger(LOG_INFO, "Requesting check of %s\n", pent->d_name);
            data = utilsLoadUUID("iot", pent->d_name);
            if ( data == NULL )
            {
                piboxLogger(LOG_ERROR, "Failed check of %s\n", pent->d_name);
                continue;
            }

            /* Compare to input arg */
            if ( strcmp(data, uuid) == 0 ) 
            {
                filename = strdup( pent->d_name );
                free(data);
                break;
            }
            free(data);
        }
        closedir( pdir );
    }

    if ( filename == NULL )
        piboxLogger(LOG_WARN, "No match for: %s\n", uuid);

    free(dirname);
    return filename;
}

/*========================================================================
 * Name:   utilsBuildResponse
 * Prototype:  void utilsBuildResponse( char *buffer, size_t size, size_t nitems, void *userdata )
 *
 * Description:
 * Inbound data is sent here by curl.  But a single response may not be a complete body of a single response.
 * So this function builds the response over one or more calls from curl.
 *
 * Input Arguments:
 * char *chunk      Partial (or all) return data from an HTTP call to a sensor. This is not NULL terminated.
 * size_t size      Always 1.
 * size_t nitems    Size of the data.
 * void *userdata   Storage for the inbound response.
 *
 * Returns:
 * 0 on failure, size*nitems (total bytes handled) on success.
 *
 * Notes:
 * This is the callback for curl calls with return data from utilsHttpPost().
 *========================================================================*/
static int
utilsBuildResponse( char *chunk, size_t size, size_t nitems, void *userdata )
{
    size_t realsize = size * nitems;
    struct response *data = (struct response *)userdata;
    char *ptr;
 
    if ( !data )
    {
        piboxLogger(LOG_ERROR, "Response data pointer is null.\n");
        return 0;
    }

    if ( data->response != NULL )
        ptr = (char *)calloc(1, strlen(data->response) + realsize + 1);
    else
        ptr = (char *)calloc(1, realsize + 1);

    if(!ptr)
    {
        piboxLogger(LOG_ERROR, "Failed to allocate new response chunk.\n");
        return 0;  /* out of memory! */
    }
 
    if ( data->response )
    {
        strcpy(ptr, data->response);
        memcpy((char *)(ptr+strlen(data->response)), chunk, realsize);
        free(data->response);
    }
    else
    {
        memcpy((char *)ptr, chunk, realsize);
    }
    data->response = ptr;
    return realsize;
}

/*========================================================================
 * Name:   utilsHttpPost
 * Prototype:  void utilsHttpPost( char *dest, char *uri, char *post_data )
 *
 * Description:
 * POST a request to a remote device.
 * Returns the body of the response as a string.
 * Caller is responsible for decrypting responses.
 *
 * Input Arguments:
 * char *dest       Remote device address
 * char *uri        API path
 * char *post_data  Data to send with request, if any.
 *
 * Returns:
 * Character string response or NULL if not response.
 *
 * Notes:
 * Mongoose is a web server, so it doesn't necesasrily make web requests easily.
 * Instead, we use libcurl to make requests.
 *========================================================================*/
void
utilsHttpPost( char *dest, char *uri, char *post_data )
{
    CURL        *curl;
    CURLcode    res;
    char        *url;
    int         port = 80;
    struct response data = {.response = NULL};

    /* In test mode, we let the sensor simulator run on a non-standard port. */
    if ( isCLIFlagSet( CLI_TEST) )
        port = HTTP_PORT_T;

    curl = curl_easy_init();
    if(curl) 
    {
        /* Build the full URL, sans POST data. */
        url = (char *)calloc(1, strlen(dest) + strlen(uri) + 14);
        sprintf(url, "http://%s:%d%s", dest, port, uri);
        piboxLogger(LOG_INFO, "URL: %s\n", url);

        /* 
         * First set the URL that is about to receive our POST. This URL can
         * just as well be an https:// URL if that is what should receive the
         * data.
         */
        curl_easy_setopt(curl, CURLOPT_URL, url);

        /* Now specify the POST data */
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, post_data);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, strlen(post_data));
        curl_easy_setopt(curl, CURLOPT_POST, 1L);

        /* Setup for handling any respnose */
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, utilsBuildResponse);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &data);

        /* Perform the request, res will get the return code */
        res = curl_easy_perform(curl);

        /* Check for errors */
        if(res != CURLE_OK)
            piboxLogger(LOG_ERROR, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));

        /* always cleanup */
        curl_easy_cleanup(curl);

        /* Cleanup */
        free(url);

        /* Handle response, if any. */
        if ( data.response != NULL )
        {
            piboxLogger(LOG_INFO, "Response data: %s\n", data.response);
            deviceSaveSensorData( data.response, dest );
            free(data.response);
            data.response = NULL;
        }
        else
            piboxLogger(LOG_INFO, "Response data is NULL\n");
    }
    else
        piboxLogger(LOG_ERROR, "Failed to init curl for POST operation.\n");

    return;
}

/*========================================================================
 * Name:   utilsGetRemoteIP
 * Prototype:  char *utilsGetRemoteIP( char * )
 *
 * Description:
 * Convert the remote IP to a string.
 *
 * Input Arguments:
 * char *requestIP    The IP address of the remote device to check.
 *
 * Returns:
 * Remote IP as a quad-formatted character string or NULL if the conversion
 * cannot be done.
 *========================================================================*/
char *
utilsGetRemoteIP( struct mg_connection *c )
{
    char    *buf;

    buf = (char *)calloc(1, 17);
    sprintf(buf, "%d.%d.%d.%d", 
                c->rem.ip[0],
                c->rem.ip[1],
                c->rem.ip[2],
                c->rem.ip[3]);
    return(buf);
}

/*========================================================================
 * Name:   utilsGenUUID
 * Prototype:  char *utilsGenUUID( void )
 *
 * Description:
 * Generate a UUID string.
 *
 * Input Arguments:
 * N/A
 *
 * Returns:
 * A 37 character string.
 *========================================================================*/
char *
utilsGenUUID( void )
{
    static uuid_t   uuid;
    char            *value;

    uuid_generate(uuid);
    value = (char *)calloc(1,37);
    uuid_unparse(uuid, value);
    return(value);
}
