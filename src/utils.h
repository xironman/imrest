/*******************************************************************************
 * imrest - a RESTful web server based on Mongoose.
 *
 * utils.h:  Utilty functions
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef UTILS_H
#define UTILS_H

/*========================================================================
 * Defined values
 *=======================================================================*/
#define LOCALHOST           "127.0.0.1"
#define DEFAULT_DESCRIPTOR  "No descriptor available."

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef UTILS_C
extern int   utilsParse(char *line, char **argv, int max);
extern char *utilsLoadUUID(char *source, char *requestIP);
extern int   utilsLocalIP(char *requestIP);
extern char *utilsGetIPv4( char *uuid );
extern char *utilsGetDescriptor( void );
extern char *utilsGetRemoteIP( struct mg_connection *c );
extern char *utilsHttpPost( char *dest, char *uri, char *post_data );
extern char *utilsGenUUID( void );
#endif /* !UTILS_C */
#endif /* !UTILS_H */
