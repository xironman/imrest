# Unit Test Programs

Files in this directory are used for testing the imrest server.

## sensor

This is a sensor simulator that accepts and responds to messages from imrest.

## jarvis

This is a jarvis simulator used to issue requests that would normally come from the voice-controlled Jarvis system.

## unittest.sh

Front end to running tests.  It calls jarvis as needed but does not run sensor, which must be run manually to handle
imrest messages to sensors.
