/*******************************************************************************
 * imrest RESTful server
 *
 * cli.h:  Command line parsing
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef CLI_H
#define CLI_H

/*========================================================================
 * Type definitions
 *=======================================================================*/
#define CLI_LOGTOFILE       0x00008    // Enable log to file
#define CLI_TEST            0x00020    // Enable test mode (read data files locally)
#define CLI_ROOT            0x00040    // Running as root

typedef struct _cli_t {
    int     flags;              // Enable/disable features
    int     verbose;            // Sets the verbosity level for the application
    char    *logFile;           // Name of local file to write log to
    char    *webroot;           // Name of data directory (re: webroot)
} CLI_T;

/*========================================================================
 * Text strings 
 *=======================================================================*/
/* Version information should be passed from the build */
#ifndef VERSTR
#define VERSTR      "No Version String"
#endif

#ifndef VERDATE
#define VERDATE     "No Version Date"
#endif

#define CLIARGS     "Tw:l:v:"
#define USAGE \
"\n\
imrest [ -T | -w <dir> | -l <filename> | -v <level> | -h? ]\n\
where\n\
\n\
    -w dir          Set webroot directory (required). \n\
    -T              Use test files (for debugging only) \n\
    -l filename     Enable local logging to named file \n\
    -v level        Enable verbose output: \n\
                    0: LOG_NONE  (default) \n\
                    1: LOG_INFO            \n\
                    2: LOG_WARN            \n\
                    3: LOG_ERROR           \n\
                    4: LOG_TRACE1          \n\
                    5: LOG_TRACE2          \n\
                    6: LOG_TRACE3          \n\
                    7: LOG_TRACE4          \n\
                    8: LOG_TRACE5          \n\
\n\
"


/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef CLI_C
CLI_T cliOptions;
#else
extern CLI_T cliOptions;
#endif /* CLI_C */

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifdef CLI_H
void parseArgs(int argc, char **argv);
void initConfig( void );
int  validateConfig( void );
int  isCLIFlagSet( int bits );
void setCLIFlag( int bits );
void unsetCLIFlag( int bits );
#else
extern void parseArgs(int argc, char **argv);
extern void initConfig( void );
extern int  validateConfig( void );
extern int  isCLIFlagSet( int bits );
extern void setCLIFlag( int bits );
extern void unsetCLIFlag( int bits );
#endif

#endif /* !CLI_H */
