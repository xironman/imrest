/*******************************************************************************
 * jarvis - Simulate Jarvis requests.
 *
 * jarvis.c:  program main
 *
 * Terminology
 * ------------------------------------------------------------------------
 * monitor:     Application running on RPi/PiBox used to manage IoT nodes.
 * IoT node:    Remote node typically running Arduino, or local app managing remote device.
 * Jarvis:      Remote voice control application that proxies through imrest.
 *
 * This simulates a Jarvis app or monitor sending requests to imrest and
 * handling the return data.  It is used for unit testing the imrest server.
 *
 * Jarvis API commands simulated:
 * GET      /monitor              Retrieve monitor decriptor file from imrest host
 * POST     /pair/jarvis          Request pairing with imrest host.
 * POST     /devices              Retrieve list of registered devices
 * POST     /set/device           Change state of remote sensor.
 * DELETE   /set/device           Delete a registered device file
 *
 * Monitor API commands simulated:
 * GET      /query/devices        Get status of all devices
 * DELETE   /set/device           Delete a registered device file
 *
 * IoT API commands simulated:
 * POST     /pair/iot             Request pairing with imrest host.
 *
 * Version is added to header, as such:
 * Accept-Version: 1.0
 *
 * License: 0BSD
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define JARVIS_C

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>
#include <curl/curl.h>
#include <pibox/pibox.h>
#include <pibox/utils.h>

#include "jarvis.h"

static char *jarvisUUID = "4d14eafa-bb64-4425-be80-56f733c36904";
static int s_signo = 0;

/*
 *========================================================================
 *========================================================================
 * STATIC FUNCTIONS
 *========================================================================
 *========================================================================
 */

/*========================================================================
 * Name:   signal_handler
 * Prototype:  void signal_handler( int )
 *
 * Description:
 * Handle signals, which just allow exiting.
 *
 * Input Arguments:
 * int signo        Signal to handle.
 *========================================================================*/
static void 
signal_handler(int signo)
{
    s_signo = signo;
}

/*
 *========================================================================
 * Name:   apiVersionMatch
 * Prototype:  char *apiVersionMatch( char *apiVersion )
 *
 * Description:
 * Tests if the specified version matches what we support.
 *
 * Returns:
 * 0 if the API version is supported.
 * 1 if the API version is not supported.
 *========================================================================
 */
static int
apiVersionMatch( char *apiVersion )
{
    return (strcmp(apiVersion, VERSTR) == 0)? 0 : 1;
}

/*
 *========================================================================
 * Name:   setDefaultHeaders
 * Prototype:  char *setDefaultHeaders( void )
 *
 * Description:
 * Set common headers for outbound messages.
 *
 * Notes:
 * Uses:
 * mg_http_reply(struct mg_connection *c, int status_code, const char *headers, const char *body_fmt, ...);
 * Where headers is a collection of "name: value\n\r" strings.
 * So this function just builds that string and returns it to the caller.
 *
 * Caller is responsible for freeing the returned string.
 *========================================================================
 */
static char *
setDefaultHeaders( void )
{
    char *buf;

    // Set the Accept-Version header.
    buf = calloc(1,strlen("Accept-Version: ")+strlen(VERSTR)+3);
    sprintf(buf, "Accept-Version: %s\r\n", VERSTR);
    return(buf);
}

/*
 *========================================================================
 * Name:   getSensorData
 * Prototype:  char *getSensorData( char *field )
 *
 * Description:
 * Retrieve the UUID for the test sensor.
 *
 * Input Arguments
 * char *field      Field to extract from sensor file.
 *                  One of uuid, state
 *
 * Returns:
 * The character string for the specified field of the only configured test sensor
 * or NULL if the directory or file don't exist or can't be read.
 *========================================================================
 */
static char *
getSensorData( char *field )
{
    char            *dirname = NULL;
    char            *filename = NULL;
    char            *value = NULL;
    char            *buf = NULL;
    char            *str = NULL;
    DIR             *pdir = NULL;
    FILE            *fd;
    struct dirent   *pent = NULL;
    struct stat     stat_buf;
    JSON_Value      *root_value;
    JSON_Object     *root_object;

    /* Open test directory */
    dirname = (char *)calloc(1, strlen(TESTETC) + strlen(IOTDIR) + 2);
    sprintf(dirname, "%s/%s", TESTETC, IOTDIR);
    pdir = opendir( dirname );

    /* Find first entry */
    if (pdir != NULL)
    {
        /* Iterate files */
        while( (pent = readdir(pdir)) != NULL )
        {   
            /* Skip non-sensor-device entries */
            if ( strncmp(pent->d_name, ".", 1) == 0 )
                continue;

            filename = (char *)calloc(1, strlen(dirname) + strlen(pent->d_name) + 2);
            sprintf(filename, "%s/%s", dirname, pent->d_name);
            break; 
        }
        closedir(pdir);
    }
    else
    {
        piboxLogger(LOG_ERROR, "Can't find test dir %s\n", dirname);
        return NULL;
    }

    if ( filename == NULL)
    {
        piboxLogger(LOG_ERROR, "Can't find a sensor file in %s\n", dirname);
        free(dirname);
        return NULL;
    }
    free(dirname);

    /* Extract UUID */
    fd = fopen(filename, "r");
    if ( !fd )
    {
        piboxLogger(LOG_ERROR, "Failed to open %s\n", filename);
        free(filename);
        return NULL;
    }
    stat(filename, &stat_buf);
    if ( stat_buf.st_size == 0 )
    {
        piboxLogger(LOG_ERROR, "%s is empty.\n", filename);
        free(filename);
        return NULL;
    }

    buf = (char *)calloc(1, stat_buf.st_size);
    if ( fread(buf, 1, stat_buf.st_size, fd) != stat_buf.st_size )
    {
        piboxLogger(LOG_ERROR, "Read failed on %s.\n", filename);
        free(filename);
        free(buf);
        return NULL;
    }
    fclose(fd);
    free(filename);

    root_value = json_parse_string(buf);
    root_object = json_value_get_object(root_value);
    free(buf);
    str = json_serialize_to_string(root_value);
    piboxLogger(LOG_INFO, "Sensor file data: %s\n", str);
    json_free_serialized_string(str);

    /* Verify the request contains the "uuid" key. */
    value = (char *)json_object_get_string( root_object, field );
    if ( value == NULL )
    {
        piboxLogger(LOG_ERROR, "Sensor file missing %s.\n", field);
        return NULL;
    }

    str = strdup(value);
    json_value_free(root_value);
    return str;
}

/*
 *========================================================================
 *========================================================================
 * UNIT TESTS
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   getMonitor
 * Prototype:  void getMonitor( void )
 *
 * Description:
 * Send a GET /monitor message to imrest and wait for it's response.
 *
 * Receives:
 * TBD
 *
 * Notes:
 * TBD
 *========================================================================
 */
void
getMonitor(void)
{
    utilsHttpGet(cliOptions.server, "/monitor", 1);
}

/*
 *========================================================================
 * Name:   postPairJarvis
 * Prototype:  void postPairJarvis(Jarvis void )
 *
 * Description:
 * Send a POST /pair/jarvis message to imrest.
 *
 * Receives:
 * 200 if registration is processed.
 * 401 if registration is rejected.
 *
 * Notes:
 * TBD
 *========================================================================
 */
void
postPairJarvis(void)
{
    JSON_Value      *root_value;
    JSON_Object     *root_object;
    char            *request;

    /* Build the JSON command object. */
    root_value = json_value_init_object();
    root_object = json_value_get_object(root_value);
    json_object_set_string( root_object, "uuid", jarvisUUID );

    /* Stringify the JSON command object */
    request = json_serialize_to_string(root_value);

    /* Send the request. */
    utilsHttpPost(cliOptions.server, "/pair/jarvis", request, 1);

    json_free_serialized_string(request);
    json_value_free(root_value);
}

/*
 *========================================================================
 * Name:   postDevices
 * Prototype:  void postDevices( void )
 *
 * Description:
 * Send a POST /devices message to imrest and wait for it's response.
 *
 * Receives:
 * 200 if all goes well.
 * 401 if request is not made from local host.
 * 500 if message encryption fails.
 *
 * Notes:
 * This tests if Jarvis can get a list of registered devices.
 *========================================================================
 */
void
postDevices(void)
{
    JSON_Value      *root_value;
    JSON_Object     *root_object;
    char            *request;
    char            *message;

    /* Build the JSON command object. */
    root_value = json_value_init_object();
    root_object = json_value_get_object(root_value);
    json_object_set_string( root_object, "command", "getDevices" );

    /* Stringify the JSON command object */
    request = json_serialize_to_string(root_value);

    /* Encrypt the JSON command object. */
    message = cryptoEncrypt(jarvisUUID, request);
    piboxLogger(LOG_INFO, "Request to send to imrest: %s\n", message);

    /* Send the request. */
    utilsHttpPost(cliOptions.server, "/devices", message, 0);

    free(message);
    json_free_serialized_string(request);
    json_value_free(root_value);
}

/*
 *========================================================================
 * Name:   postSetDevice
 * Prototype:  void postSetDevice( void )
 *
 * Description:
 * Send a POST /set/device message to imrest and wait for it's response.
 *
 * Receives:
 * 200 if all goes well.
 * 400 if message cannot be authorized.
 * 401 if message does not contain "update".
 * 500 if message encryption fails.
 *
 * Notes:
 * Jarvis build the URL as /set/device/
 * The outbound message is JSON base64 encoded with IV and AES encrypted message fields.
 * The message field is JSON containing
 * 1. uuid field    - The UUID of the sensor node to update (uuid:<>)
 * 2. state field   - The new state of the sensor (state:<>).
 * 3. command field - The command, which must be "update" ( message:"update")
 *========================================================================
 */
void
postSetDevice(void)
{
    char            *uuid;
    char            *state;
    char            *message;
    char            *request;
    char            *newVal;
    JSON_Value      *root_value;
    JSON_Object     *root_object;

    /* Get the UUID of a sensor node to update. */
    uuid = getSensorData("uuid");
    if ( uuid == NULL )
    {
        piboxLogger(LOG_ERROR, "Can't get sensor UUID.\n");
        return;
    }
    state = getSensorData("state");
    if ( state == NULL )
    {
        piboxLogger(LOG_ERROR, "Can't get sensor state.\n");
        return;
    }
    if ( strcmp(state, "0") == 0 )
        newVal = "1";
    else
        newVal = "0";

    /* Build the JSON command object. */
    root_value = json_value_init_object();
    root_object = json_value_get_object(root_value);
    json_object_set_string( root_object, "uuid", uuid );
    json_object_set_string( root_object, "command", "update" );
    json_object_set_string( root_object, "state", newVal );

    /* Stringify the JSON command object */
    request = json_serialize_to_string(root_value);

    /* Encrypt the JSON command object. */
    message = cryptoEncrypt(jarvisUUID, request);
    piboxLogger(LOG_INFO, "Request to send to imrest: %s\n", message);

    /* Send the request. */
    utilsHttpPost(cliOptions.server, "/set/device", message, 0);

    free(message);
    json_free_serialized_string(request);
    json_value_free(root_value);
    free(uuid);
}

/*
 *========================================================================
 * Name:   deleteSetDevice
 * Prototype:  void deleteSetDevice( int type )
 *
 * Description:
 * Send a DELETE /set/device message to imrest and wait for it's response.
 *
 * Input Arguments:
 * int type     One of JARVIS_T or MONITOR_T
 *
 * Receives:
 * TBD
 *
 * Notes:
 * This command can come from either a Jarvis node or the monitor.
 *========================================================================
 */
void
deleteSetDevice(int type)
{
    char            *uuid;
    char            *message;
    char            *request;
    JSON_Value      *root_value;
    JSON_Object     *root_object;

    /* Check if test is for Monitor, which must be same IP as imrest server. */
    if ( type == MONITOR_T )
    {
        if ( utilsCompareToServerIP() == 0 )
        {
            piboxLogger(LOG_ERROR, "Monitor tests must be run on same host as imrest.\n");
            return;
        }
    }

    /* Get the UUID of a sensor node to delete. */
    uuid = getSensorData("uuid");
    if ( uuid == NULL )
    {
        piboxLogger(LOG_ERROR, "Can't get sensor UUID.\n");
        return;
    }

    /* Build the JSON command object. */
    root_value = json_value_init_object();
    root_object = json_value_get_object(root_value);
    json_object_set_string( root_object, "uuid", uuid );
    json_object_set_string( root_object, "command", "delete" );

    /* Stringify the JSON command object */
    request = json_serialize_to_string(root_value);

    /* Encrypt the JSON command object. */
    message = cryptoEncrypt(jarvisUUID, request);
    piboxLogger(LOG_INFO, "Request to send to imrest: %s\n", message);

    /* Send the request. */
    utilsHttpDelete(cliOptions.server, "/set/device", message);

    free(message);
    json_free_serialized_string(request);
    json_value_free(root_value);
    free(uuid);
}

/*
 *========================================================================
 * Name:   getQueryDevices
 * Prototype:  void getQueryDevices( void )
 *
 * Description:
 * Send a GET /query/devices message to imrest and wait for it's response.
 *
 * Receives:
 * 200 if all goes well.
 * 401 if request is not made from local host.
 * 500 if request can't be processed (can't get to dirs or files).
 *
 * Notes:
 * This tests if the monitor can make requests to get updated status from
 * all registered IoT nodes.
 * This API does not require a JSON body nor does it require anything
 * to be encrypted or encoded.
 *========================================================================
 */
void
getQueryDevices(void)
{
    /* Send the request. */
    utilsHttpGet(cliOptions.server, "/query/devices", 0);
}

/*
 *========================================================================
 * Name:   postPairIot
 * Prototype:  void postPairIot( void )
 *
 * Description:
 * Send a POST /pair/iot message to imrest.
 *
 * Receives:
 * 200 if registration is processed.
 * 401 if registration is rejected.
 *
 * Notes:
 * TBD
 *========================================================================
 */
void
postPairIot(void)
{
    JSON_Value      *root_value;
    JSON_Object     *root_object;
    char            *request;
    char            *uuid;

    uuid = utilsGenUUID();

    /* Build the JSON command object. */
    root_value = json_value_init_object();
    root_object = json_value_get_object(root_value);
    json_object_set_string( root_object, "uuid", uuid );

    /* Sample IoT node is a light switch */
    json_object_set_string( root_object, "type", "light_switch" );
    json_object_set_string( root_object, "description", "Toggle light switch" );
    json_object_set_string( root_object, "state", "0" );

    /* Stringify the JSON command object */
    request = json_serialize_to_string(root_value);

    /* Send the request. */
    utilsHttpPost(cliOptions.server, "/pair/iot", request, 1);

    json_free_serialized_string(request);
    json_value_free(root_value);
    free(uuid);
}

/*
 *========================================================================
 *========================================================================
 * MAIN
 *========================================================================
 *========================================================================
 */

/*
 *========================================================================
 * Name:   main
 * Prototype:  int main( int argc, char **argv )
 *
 * Description:
 * Set common headers for outbound messages.
 *
 * Notes:
 * Uses:
 * mg_http_reply(struct mg_connection *c, int status_code, const char *headers, const char *body_fmt, ...);
 * Where headers is a collection of "name: value\n\r" strings.
 * So this function just builds that string and returns it to the caller.
 *
 * Caller is responsible for freeing the returned string.
 *========================================================================
 */
int
main(int argc, char *argv[])
{
    /* Load saved configuration and parse command line */
    initConfig();
    parseArgs(argc, argv);

    /* Setup logging */
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);
    printf("Verbosity level: %d\n", piboxLoggerGetVerbosity());

    if ( cliOptions.logFile != NULL )
    {
        piboxLogger(LOG_INFO, "Log file: %s\n", cliOptions.logFile);
    }
    else
    {
        piboxLogger(LOG_INFO, "No log file configured.\n");
    }

    /* Make sure configured options are okay. */
    if ( validateConfig() != 0 )
    {
        piboxLogger(LOG_ERROR, "Configuration failure.\n");
        goto skipit;
    }

    /* Setup signal handling */
    signal(SIGINT, signal_handler);
    signal(SIGTERM, signal_handler);

    /* Initialize libcurl. */
    curl_global_init(CURL_GLOBAL_ALL);

    /* Handle test request. */
    switch (cliOptions.test )
    {
        case 1: getMonitor();               /* Jarvis: GET /monitor */
            break;

        case 2: postPairJarvis();           /* Jarvis: POST /pair/jarvis */
            break;

        case 3: postDevices();              /* Jarvis: POST /devices */
            break;

        case 4: postSetDevice();            /* Jarvis: POST /set/device */
            break;

        case 5: deleteSetDevice(JARVIS_T);  /* Jarvis: DELETE /set/device */
            break;

        case 6: getQueryDevices();          /* Monitor: GET /query/devices */
            break;

        case 7: deleteSetDevice(MONITOR_T); /* Monitor: DELETE /set/device */
            break;

        case 8: postPairIot();              /* Jarvis: POST /pair/iot */
            break;
    }

skipit:
    piboxLoggerShutdown();
    return 0;
}
