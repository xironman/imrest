/*******************************************************************************
 * jarvis - Simulate Jarvis requests.
 *
 * jarvis.h:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef JARVIS_H
#define JARVIS_H

/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef JARVIS_C
#endif /* JARVIS_C */

/*========================================================================
 * Defined values
 *=======================================================================*/
#define PROG        "jarvis"
#define MAXBUF      4096
#define JARVIS_T    1
#define MONITOR_T   2

#define TESTETC     "data/etc"
#define IOTDIR      "ironman/iot"
#define JARVISDIR   "ironman/jarvis"
#define MONDIR      "monitor"

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef JARVIS_C
#endif

/*========================================================================
 * Include other headers
 *=======================================================================*/
#include <pibox/log.h>
#include <pibox/parson.h>
#include "cli-jarvis.h"
#include "base64.h"
#include "aes.h"
#include "pkcs7_padding.h"
#include "crypto.h"
#include "utils.h"

#endif /* !JARVIS_H */
