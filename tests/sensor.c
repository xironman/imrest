/*******************************************************************************
 * sensor - a RESTful web server that simulates an Arduino sensor node.
 *
 * sensor.c:  program main
 *
 * The format for REST requests is
 *
 * POST /set        From imrest node    Change a state on the sensor
 * POST /query      From imrest node    Request for current state of sensor
 * GET  /register   From piboxd node    Piboxd sends us a UUID in response to the
 *                                      sensors multicast request to register.
 *
 * This just simulates the RESTful API on the sensor node, to validate inbound
 * messages.  To send messages to the imrest server use the unittest.sh script.
 *
 * Version is added to header, as such:
 * Accept-Version: 1.0
 *
 * Additionally, JSON data may be included.  This would be data
 * specific to the cmd.
 *
 * License: 0BSD
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define SENSOR_C

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>
#include <curl/curl.h>
#include <pibox/pibox.h>
#include <pibox/utils.h>

#include "sensor.h"

static char *uuid = "105516fb-b8ec-4393-a820-f7b5aa227449";
static int s_signo = 0;

/*========================================================================
 * Name:   signal_handler
 * Prototype:  void signal_handler( int )
 *
 * Description:
 * Handle signals, which just allow exiting.
 *
 * Input Arguments:
 * int signo        Signal to handle.
 *========================================================================*/
static void 
signal_handler(int signo)
{
    s_signo = signo;
}

/*========================================================================
 * Name:   genJSON
 * Prototype:  char *genJSON( char *, char *, char *, char * )
 *
 * Description:
 * Generate a JSON object to be used in a sensor's response to a /set or 
 * /query request.
 *
 * Input Arguments:
 * char *uuid       The UUID to use.
 * char *type       The TYPE to use.
 * char *desc       The DESCRIPTION to use.
 * char *state      The STATE to use.
 *
 * Returns:
 * The JSON object as a string.  Caller is responsible for freeing the
 * return buffer.
 *========================================================================*/
char *
genJSON( char *uuid, char *type, char *desc, char *state )
{
    char            *response;
    JSON_Value      *root_value;
    JSON_Object     *root_object;

    root_value = json_value_init_object();
    root_object = json_value_get_object(root_value);
    json_object_set_string( root_object, "uuid", uuid );
    json_object_set_string( root_object, "description", desc );
    json_object_set_string( root_object, "type", type );
    json_object_set_string( root_object, "state", state );
    response = json_serialize_to_string(root_value);
    json_value_free(root_value);
    return response;
}

/*
 *========================================================================
 * Name:   apiVersionMatch
 * Prototype:  char *apiVersionMatch( char *apiVersion )
 *
 * Description:
 * Tests if the specified version matches what we support.
 *
 * Returns:
 * 0 if the API version is supported.
 * 1 if the API version is not supported.
 *========================================================================
 */
static int
apiVersionMatch( char *apiVersion )
{
    return (strcmp(apiVersion, VERSTR) == 0)? 0 : 1;
}

/*
 *========================================================================
 * Name:   setDefaultHeaders
 * Prototype:  char *setDefaultHeaders( void )
 *
 * Description:
 * Set common headers for outbound messages.
 *
 * Notes:
 * Uses:
 * mg_http_reply(struct mg_connection *c, int status_code, const char *headers, const char *body_fmt, ...);
 * Where headers is a collection of "name: value\n\r" strings.
 * So this function just builds that string and returns it to the caller.
 *
 * Caller is responsible for freeing the returned string.
 *========================================================================
 */
static char *
setDefaultHeaders( void )
{
    char *buf;

    // Set the Accept-Version header.
    buf = calloc(1,strlen("Accept-Version: ")+strlen(VERSTR)+3);
    sprintf(buf, "Accept-Version: %s\r\n", VERSTR);
    return(buf);
}

/*
 *========================================================================
 * Name:   handleGet
 * Prototype:  void *handleGet( struct mg_connection *c, struct mg_http_message *hm )
 *
 * Description:
 * Front end to handling GET requests.
 *========================================================================
 */
static void
handleGet( struct mg_connection *c, struct mg_http_message *hm )
{
    char    *hdrs;

    if (mg_http_match_uri(hm, "/register"))
    {
        hdrs = setDefaultHeaders();
        piboxLogger(LOG_INFO, "/register: Received registration.\n");
        mg_http_reply(c, 200, "", "\n");
        free(hdrs);
    }
}

/*
 *========================================================================
 * Name:   handlePost
 * Prototype:  void *handlePost( struct mg_http_message *hm )
 *
 * Description:
 * Front end to handling POST requests.
 *========================================================================
 */
static void
handlePost( struct mg_connection *c, struct mg_http_message *hm )
{
    char    *uri;
    char    *body;
    char    *request;
    char    *response;
    char    *encrypted;
    char    *state;
    char    *value;
    JSON_Value      *root_value;     // Data block wrapping JSON objects
    JSON_Object     *root_object;    // root JSON object

    uri = (char *)calloc(1, hm->uri.len + 1 );
    sprintf(uri, "%.*s", (int)hm->uri.len, hm->uri.ptr);

    body = (char *)calloc(1, hm->body.len + 1 );
    sprintf(body, "%.*s", (int)hm->body.len, hm->body.ptr);

    if (mg_http_match_uri(hm, "/set"))
    {
        piboxLogger(LOG_INFO, "/set: Received body: %s\n", body);
        request = cryptoDecrypt(uuid, body);
        if ( request == NULL )
            goto handle_post_error;
        piboxLogger(LOG_INFO, "/set request: %s\n", request);
        root_value = json_parse_string(request);
        if ( root_value == NULL )
        {
            piboxLogger(LOG_ERROR, "Failed to parse request: %s\n", request);
            goto handle_post_error;
        }
        root_object = json_value_get_object(root_value);
        state = (char *)json_object_get_string( root_object, "state" );
        if ( state == NULL )
        {
            piboxLogger(LOG_ERROR, "Failed to extract state field from json.\n");
            goto handle_post_error;
        }
        piboxLogger(LOG_INFO, "Requested state: %s\n", state);

        response = genJSON(uuid, "light_switch", "Light Description Goes Here", state);
        value = cryptoEncrypt(uuid, response);
        if ( value == NULL )
        {
            piboxLogger(LOG_ERROR, "Failed to encrypt response.\n");
            goto handle_post_error;
        }
        encrypted = (char *)calloc(1, strlen(value) + 2 );
        sprintf(encrypted, "%s\n", value);
        mg_http_reply(c, 200, "Content-Type: application/json\r\n", encrypted);
        free(encrypted);
        json_free_serialized_string(value);
        json_free_serialized_string(response);
        json_value_free(root_value);
    }
    else if (mg_http_match_uri(hm, "/query"))
    {
        /* We don't do the same validation here as a sensor would. */
        piboxLogger(LOG_INFO, "/query: Received body: %s\n", body);
        request = cryptoDecrypt(uuid, body);
        if ( request == NULL )
            goto handle_post_error;
        piboxLogger(LOG_INFO, "/set request: %s\n", request);
        if ( strncmp(request, "getdevices", 10) != 0 )
        {
            piboxLogger(LOG_ERROR, "Invalid request for /query.\n");
            goto handle_post_error;
        }
        response = genJSON(uuid, "light_switch", "Light Description Goes Here", "1");
        value = cryptoEncrypt(uuid, response);
        if ( value == NULL )
        {
            piboxLogger(LOG_ERROR, "Failed to encrypt response.\n");
            goto handle_post_error;
        }
        encrypted = (char *)calloc(1, strlen(value) + 2 );
        sprintf(encrypted, "%s\n", value);
        mg_http_reply(c, 200, "Content-Type: application/json\r\n", encrypted);
        free(encrypted);
        free(value);
        free(response);
        free(request);
    }
    else
    {
        /* All other requests are unauthorized. */
        mg_http_reply(c, 401, NULL, "ok\n");
    }
    goto handle_post_exit;

handle_post_error:
    mg_http_reply(c, 400, NULL, "\n");

handle_post_exit:
    free(body);
    free(uri);
}

/*
 *========================================================================
 * Name:   eventHandler
 * Prototype:  void eventHandler( void )
 *
 * Description:
 * Handler called by mg_mgr_poll() when inbound messages arrive.
 * This just tests the HTTP method and passes the messages to the appropriate
 * method handlers.
 *========================================================================
 */
static void
frontEnd(struct mg_connection *c, int ev, void *ev_data)
{
    char    *method;

    if (ev == MG_EV_HTTP_MSG) 
    {
        // The MG_EV_HTTP_MSG event means HTTP request. `hm` holds parsed request,
        // see https://mongoose.ws/documentation/#struct-mg_http_message
        struct mg_http_message *hm = (struct mg_http_message *) ev_data;

        method = (char *)calloc(1, hm->method.len + 1 );
        sprintf(method, "%.*s", (int)hm->method.len, hm->method.ptr);

        piboxLogger(LOG_INFO, "Inbound method: %s\n", method);
        if ( strcmp(method, "GET") == 0 )
            handleGet(c, hm);
        else if ( strcmp(method, "POST") == 0 )
            handlePost(c, hm);
        else 
        {
            piboxLogger(LOG_ERROR, "Unsupported HTTP method: %s\n", method);
        }

        free(method);
    }
}

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * Program startup
 *
 * Notes:
 * This is a RESTful web server based on Mongoose.
 * It's purpose is to provide a web inteface for sensors and Jarvis to
 * communication with Ironman's monitor.
 * ========================================================================
 */
int
main(int argc, char *argv[])
{
    struct mg_mgr   mgr;
    char            server_url[128];

    /* Load saved configuration and parse command line */
    initConfig();
    parseArgs(argc, argv);

    /* Setup logging */
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);
    printf("Verbosity level: %d\n", piboxLoggerGetVerbosity());

    if ( cliOptions.logFile != NULL )
    {
        piboxLogger(LOG_INFO, "Log file: %s\n", cliOptions.logFile);
    }
    else
    {
        piboxLogger(LOG_INFO, "No log file configured.\n");
    }

    /* Make sure configured options are okay. */
    if ( validateConfig() != 0 )
    {
        piboxLogger(LOG_ERROR, "Configuration failure.\n");
        goto skipit;
    }

    /* Setup signal handling */
    signal(SIGINT, signal_handler);
    signal(SIGTERM, signal_handler);

    /* Initialize libcurl. */
    curl_global_init(CURL_GLOBAL_ALL);

    /* Initialize Mongoose. */
    mg_mgr_init(&mgr);        // Init manager
    mg_log_set(MG_LL_DEBUG);  // Set debug log level. Default is MG_LL_INFO
    sprintf(server_url, "http://0.0.0.0:%d", HTTP_PORT);
    mg_http_listen(&mgr, server_url, frontEnd, NULL);  // Setup listener

    /* Spin waiting for connections. */
    while ( s_signo == 0 )
        mg_mgr_poll(&mgr, 1000);  

    /* Clean up Mongoose. */
    mg_mgr_free(&mgr);

skipit:
    piboxLoggerShutdown();
    return 0;
}
