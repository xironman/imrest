/*******************************************************************************
 * jarvis - Simulate Jarvis requests.
 *
 * utils.c:  Various utility methods
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define UTILS_C

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <net/if.h>
#include <unistd.h>
#include <uuid.h>
#include <arpa/inet.h>
#include <curl/curl.h>
#include <pibox/pibox.h>
#include <pibox/utils.h>

#include "jarvis.h"

struct response {
    char    *response;
};

/* Local prototypes. */
char *utilsGenUUID( void );

/* piboxd listens on this port */
#define PIBOXD_PORT 13910

/*========================================================================
 * Name:   utilsLoadUUID
 * Prototype:  char *utilsLoadUUID( char *, char * )
 *
 * Description:
 * Load a devices UUID from it's stored file.
 *
 * Input Arguments:
 * char *source       One of "jarvis" or "iot"
 * char *requestIP    The IP address of the device or Jarvis node.
 *
 * Returns:
 * A UUID string or NULL if the string can't be retrieved.
 * The caller is required to free the returned string.
 *========================================================================*/
char *
utilsLoadUUID(char *source, char *requestIP)
{
    char            *dirName;
    char            *filename;
    char            *value;
    char            *uuid = NULL;
    char            *data;
    struct stat     stat_buf;
    FILE            *fd;
    JSON_Value      *root_value;
    JSON_Object     *root_object;

    /* Open test directory */
    if ( strcmp(source,"jarvis") == 0 )
    {   
        dirName = (char *)calloc(1, strlen(TESTETC) + strlen(JARVISDIR) + 2);
        sprintf(dirName, "%s/%s", TESTETC, JARVISDIR);
    }
    else
    {   
        dirName = (char *)calloc(1, strlen(TESTETC) + strlen(IOTDIR) + 2);
        sprintf(dirName, "%s/%s", TESTETC, IOTDIR);
    }

    filename = (char *)calloc(1, strlen(dirName) + strlen(requestIP) + 2);
    sprintf(filename, "%s/%s", dirName, requestIP);
    piboxLogger(LOG_INFO, "filename: %s\n", filename);
    free(dirName);

    if ( stat(filename, &stat_buf) == 0 )
    {   
        fd = fopen(filename, "r");
        if ( fd )
        {
            data = (char *)calloc(1, stat_buf.st_size + 1);
            if ( fread(data, 1, stat_buf.st_size, fd) == stat_buf.st_size )
            {
                piboxLogger(LOG_INFO, "File data: %s\n", data);
                root_value = json_parse_string(data);
                root_object = json_value_get_object(root_value);
                value = (char *)json_object_get_string( root_object, "uuid" );
                if ( value )
                    uuid = strdup(value);
                json_value_free(root_value);
            }
            else
                piboxLogger(LOG_ERROR, "Failed to read %s file: %s\n", source, filename);
            free(data);
            fclose(fd);
        }
        else
            piboxLogger(LOG_ERROR, "Failed to open %s file: %s\n", source, filename);
    }
    else
        piboxLogger(LOG_ERROR, "Failed to find %s file: %s\n", source, filename);

    free(filename);
    return uuid;
}

/*========================================================================
 * Name:   parseResponse
 * Prototype:  void parseResponse( char *json, char *ipaddr, int monitor )
 *
 * Description:
 * A complete response is decoded here.
 *
 * Input Arguments:
 * char *json       Return data from an HTTP call to a remote node. This is not NULL terminated.
 * void *ipaddr     Storage for the inbound response.
 * int  monitor     If set, use jarvisUUID for decoding.
 *
 * Returns:
 * N/A
 *
 * Notes:
 * N/A
 *========================================================================*/
void
parseResponse( char *json, char *ipaddr, int monitor )
{
    char            *uuid;
    char            *message = NULL;

    piboxLogger(LOG_INFO, "Entered.\n");
    if ( json == NULL )
    {
        piboxLogger(LOG_INFO, "json == NULL.\n");
        return;
    }

    /* If monitor is set we need don't need to decode/decrypt */
    if ( !monitor )
    {
        /* Grab the UUID of the node providing the data. */
        uuid = utilsLoadUUID("iot", ipaddr);
        if ( uuid == NULL )
        {
            uuid = utilsLoadUUID("jarvis", ipaddr);
            if ( uuid == NULL )
            {
                piboxLogger(LOG_INFO, "Skipping response from unrecognized node: %s\n", ipaddr);
                return;
            }
        }

        /* Decode the response */
        message = cryptoDecryptJson(uuid, json);
        if ( message == NULL )
        {
            piboxLogger(LOG_INFO, "Unable to decrypt message from %s\n", ipaddr);
            free(uuid);
            return;
        }
        piboxLogger(LOG_INFO, "Response: %s\n", message);
        free(message);
        free(uuid);
    }
    else
    {
        if ( strlen(json) > 0 )
            piboxLogger(LOG_INFO, "Response: %s\n", json);
        else
            piboxLogger(LOG_INFO, "Empty Response.\n");
    }
}

/*========================================================================
 * Name:   utilsCompareToServerIP
 * Prototype:  int utilsCompareToServerIP( void )
 *
 * Description:
 * Test if local IP is the same as the remote imrest server.
 *
 * Input Arguments:
 * N/A
 *
 * Returns:
 * 0 if the local host has a different IP as the imrest server.
 * 1 if the local host is the same as the imrest server.
 *========================================================================*/
int
utilsCompareToServerIP( void )
{
    int fd;
    int i=0;
    int rc=0;
    struct ifreq ifr;
    char ipaddr[32];
    DIR *pdir = NULL;
    struct dirent *pent = NULL;

    pdir = opendir( "/sys/class/net" );
    if (pdir != NULL)
    {
        /* Iterate files */
        while( (pent = readdir(pdir)) != NULL )
        {
            /* Get a socket to use with the ioctl. */
            fd = socket(AF_INET, SOCK_DGRAM, 0);
            memset(&ifr, 0, sizeof(ifr));
            ifr.ifr_addr.sa_family = AF_INET;

            /* Copy the interface name in the ifreq structure */
            memcpy(ifr.ifr_name , pent->d_name , IFNAMSIZ-1);

            /* Issue ioctl to get IP addresses. */
            if ( ioctl(fd, SIOCGIFADDR, &ifr) != 0 )
            {
                continue;
            }

            /* Close the socket. */
            close(fd);

            memset(ipaddr, 0 , 32);
            strcpy(ipaddr, inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr) );
            if ( strcmp(cliOptions.server, ipaddr) == 0 )
            {
                rc=1;
                break;
            }
            i++;
        }
        closedir(pdir);
    }
    return rc;
}

/*========================================================================
 * Name:   utilsLocalIP
 * Prototype:  int utilsLocalIP( char * )
 *
 * Description:
 * Test if an IP is from the local host (re: loopback or assigned IP).
 *
 * Input Arguments:
 * char *requestIP    The IP address of the remote device to check.
 *
 * Returns:
 * 0 if the requestIP is not from the local host.
 * 1 if the requestIP is from the local host.
 *========================================================================*/
int
utilsLocalIP(char *requestIP)
{
    int fd;
    int i=0;
    int rc=0;
    struct ifreq ifr;
    char ipaddr[32];
    DIR *pdir = NULL;
    struct dirent *pent = NULL;

    piboxLogger(LOG_INFO, "Checking %s\n", requestIP);
    if ( strcmp(requestIP, LOCALHOST) == 0 )
        return 1;

    pdir = opendir( "/sys/class/net" );
    if (pdir != NULL)
    {
        /* Iterate files */
        while( (pent = readdir(pdir)) != NULL )
        {
            /* Get a socket to use with the ioctl. */
            fd = socket(AF_INET, SOCK_DGRAM, 0);
            memset(&ifr, 0, sizeof(ifr));
            ifr.ifr_addr.sa_family = AF_INET;

            /* Copy the interface name in the ifreq structure */
            memcpy(ifr.ifr_name , pent->d_name , IFNAMSIZ-1);

            /* Issue ioctl to get IP addresses. */
            if ( ioctl(fd, SIOCGIFADDR, &ifr) != 0 )
            {
                continue;
            }

            /* Close the socket. */
            close(fd);

            memset(ipaddr, 0 , 32);
            strcpy(ipaddr, inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr) );
            if ( strcmp(requestIP, ipaddr) == 0 )
            {
                rc=1;
                break;
            }
            i++;
        }
        closedir(pdir);
    }
    return rc;
}

/*========================================================================
 * Name:   utilsBuildResponse
 * Prototype:  void utilsBuildResponse( char *buffer, size_t size, size_t nitems, void *userdata )
 *
 * Description:
 * Inbound data is sent here by curl.  But a single response may not be a complete body of a single response.
 * So this function builds the response over one or more calls from curl.
 *
 * Input Arguments:
 * char *chunk      Partial (or all) return data from an HTTP call to a sensor. This is not NULL terminated.
 * size_t size      Always 1.
 * size_t nitems    Size of the data.
 * void *userdata   Storage for the inbound response.
 *
 * Returns:
 * 0 on failure, size*nitems (total bytes handled) on success.
 *
 * Notes:
 * This is the callback for curl calls with return data from utilsHttpPost().
 *========================================================================*/
static int
utilsBuildResponse( char *chunk, size_t size, size_t nitems, void *userdata )
{
    size_t realsize = size * nitems;
    struct response *data = (struct response *)userdata;
    char *ptr;
 
    if ( !data )
    {
        piboxLogger(LOG_ERROR, "Response data pointer is null.\n");
        return 0;
    }

    if ( data->response != NULL )
        ptr = (char *)calloc(1, strlen(data->response) + realsize + 1);
    else
        ptr = (char *)calloc(1, realsize + 1);

    if(!ptr)
    {
        piboxLogger(LOG_ERROR, "Failed to allocate new response chunk.\n");
        return 0;  /* out of memory! */
    }
 
    if ( data->response )
    {
        strcpy(ptr, data->response);
        memcpy((char *)(ptr+strlen(data->response)), chunk, realsize);
        free(data->response);
    }
    else
    {
        memcpy((char *)ptr, chunk, realsize);
    }
    data->response = ptr;
    return realsize;
}

/*========================================================================
 * Name:   utilsGenUUID
 * Prototype:  char *utilsGenUUID( void )
 *
 * Description:
 * Generate a UUID string.
 *
 * Input Arguments:
 * N/A
 *
 * Returns:
 * A 37 character string.
 *========================================================================*/
char *
utilsGenUUID( void )
{
    static uuid_t   uuid;
    char            *value;

    uuid_generate(uuid);
    value = (char *)calloc(1,37);
    uuid_unparse(uuid, value);
    return(value);
}

/*========================================================================
 * Name:   utilsHttpPost
 * Prototype:  void utilsHttpPost( char *dest, char *uri, char *post_data, int monitor )
 *
 * Description:
 * POST a request to a remote device.
 * Returns the body of the response as a string.
 * Caller is responsible for decrypting responses.
 *
 * Input Arguments:
 * char *dest       Remote device address
 * char *uri        API path
 * char *post_data  Data to send with request, if any.
 * int  monitor     If set, decode using jarvisUUID.
 *
 * Returns:
 * Character string response or NULL if not response.
 *
 * Notes:
 * N/A
 *========================================================================*/
void
utilsHttpPost( char *dest, char *uri, char *post_data, int monitor )
{
    CURL        *curl;
    CURLcode    res;
    char        *url;
    int         port = HTTP_PORT;
    struct response data = {.response = NULL};

    if ( isCLIFlagSet( CLI_TEST) )
        port = HTTP_PORT_T;

    curl = curl_easy_init();
    if(curl) 
    {
        /* Build the full URL. */
        url = (char *)calloc(1, strlen(dest) + strlen(uri) + 10);
        sprintf(url, "http://%s:%d%s", dest, port, uri);
        piboxLogger(LOG_INFO, "URL: %s\n", url);

        /* 
         * First set the URL that is about to receive our POST. This URL can
         * just as well be an https:// URL if that is what should receive the
         * data.
         */
        curl_easy_setopt(curl, CURLOPT_URL, url);

        /* Now specify the POST data */
        if ( post_data )
        {
            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, post_data);
            curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, strlen(post_data));
            curl_easy_setopt(curl, CURLOPT_POST, 1L);
        }

        /* Setup for handling any respnose */
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, utilsBuildResponse);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &data);

        /* Perform the request, res will get the return code */
        res = curl_easy_perform(curl);

        /* Check for errors */
        if(res != CURLE_OK)
            piboxLogger(LOG_ERROR, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));

        /* always cleanup */
        curl_easy_cleanup(curl);

        if ( data.response != NULL )
        {
            piboxStripNewline(data.response);
            piboxLogger(LOG_INFO, "Response data: %s\n", data.response);
            parseResponse( data.response, dest, monitor );
            free(data.response);
            data.response = NULL;
        }
        else
            piboxLogger(LOG_INFO, "Response data is NULL\n");
    }
    else
        piboxLogger(LOG_ERROR, "Failed to init curl for POST operation.\n");

    return;
}

/*========================================================================
 * Name:   utilsHttpGet
 * Prototype:  void utilsHttpGet( char *dest, char *uri, int monitor )
 *
 * Description:
 * Submit a GET request to a imrest server.
 * Returns the body of the response as a string.
 * Caller is responsible for decrypting responses.
 *
 * Input Arguments:
 * char *dest       Remote device address
 * char *uri        API path
 * int  monitor     If set, decode using jarvisUUID.
 *
 * Returns:
 * Character string response or NULL if not response.
 *
 * Notes:
 * N/A
 *========================================================================*/
void
utilsHttpGet( char *dest, char *uri, int monitor )
{
    CURL        *curl;
    CURLcode    res;
    char        *url;
    int         port = HTTP_PORT;
    struct response data = {.response = NULL};

    if ( isCLIFlagSet( CLI_TEST) )
        port = HTTP_PORT_T;

    curl = curl_easy_init();
    if(curl)
    {
        /* Build the full URL, sans POST data. */
        url = (char *)calloc(1, strlen(dest) + strlen(uri) + 10);
        sprintf(url, "http://%s:%d%s", dest, port, uri);
        piboxLogger(LOG_INFO, "URL: %s\n", url);

        /*
         * First set the URL that is about to receive our GET. This URL can
         * just as well be an https:// URL if that is what should receive the
         * data.
         */
        curl_easy_setopt(curl, CURLOPT_URL, url);

        /* Setup for handling any respnose */
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, utilsBuildResponse);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &data);

        /* Perform the request, res will get the return code */
        res = curl_easy_perform(curl);

        /* Check for errors */
        if(res != CURLE_OK)
            piboxLogger(LOG_ERROR, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));

        /* always cleanup */
        curl_easy_cleanup(curl);

        if ( data.response != NULL )
        {
            piboxStripNewline(data.response);
            piboxLogger(LOG_INFO, "Response data: %s\n", data.response);
            parseResponse( data.response, dest, monitor );
            free(data.response);
            data.response = NULL;
        }
        else
            piboxLogger(LOG_INFO, "Response data is NULL\n");
    }
    else
        piboxLogger(LOG_ERROR, "Failed to init curl for POST operation.\n");

    return;
}

/*========================================================================
 * Name:   utilsHttpDelete
 * Prototype:  void utilsHttpDelete( char *dest, char *uri, char *post_data )
 *
 * Description:
 * Submit a DELETE request to a imrest server.
 * Returns the body of the response as a string.
 * Caller is responsible for decrypting responses.
 *
 * Input Arguments:
 * char *dest       Remote device address
 * char *uri        API path
 *
 * Returns:
 * Character string response or NULL if not response.
 *
 * Notes:
 * N/A
 *========================================================================*/
void
utilsHttpDelete( char *dest, char *uri, char *post_data )
{
    CURL                *curl;
    CURLcode            res;
    char                *url;
    int                 port = HTTP_PORT;
    struct curl_slist   *headers = NULL;
    struct response     data = {.response = NULL};

    if ( isCLIFlagSet( CLI_TEST) )
        port = HTTP_PORT_T;

    curl = curl_easy_init();
    if(curl)
    {
        /* Build the full URL. */
        url = (char *)calloc(1, strlen(dest) + strlen(uri) + 10);
        sprintf(url, "http://%s:%d%s", dest, port, uri);
        piboxLogger(LOG_INFO, "URL: %s\n", url);

        /*
         * First set the URL that is about to receive our DELETE. This URL can
         * just as well be an https:// URL if that is what should receive the
         * data.
         */
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_easy_setopt(curl, CURLOPT_URL, url);

        /* Now specify the POST data */
        if ( post_data )
        {
            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, post_data);
            curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, strlen(post_data));
        }

        headers = curl_slist_append(headers, "content-type: application/json");
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

        /* Setup for handling any respnose */
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, utilsBuildResponse);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &data);

        /* Perform the request, res will get the return code */
        res = curl_easy_perform(curl);

        /* Check for errors */
        if(res != CURLE_OK)
            piboxLogger(LOG_ERROR, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));

        /* always cleanup */
        curl_slist_free_all(headers);
        curl_easy_cleanup(curl);

        if ( data.response != NULL )
        {
            piboxLogger(LOG_INFO, "Response data: %s\n", data.response);
            parseResponse( data.response, dest, 0 );
            free(data.response);
            data.response = NULL;
        }
        else
            piboxLogger(LOG_INFO, "Response data is NULL\n");
    }
    else
        piboxLogger(LOG_ERROR, "Failed to init curl for POST operation.\n");

    return;
}
