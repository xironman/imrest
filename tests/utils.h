/*******************************************************************************
 * jarvis - Simulate Jarvis requests.
 *
 * utils.h:  Utilty functions
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef UTILS_H
#define UTILS_H

/*========================================================================
 * Defined values
 *=======================================================================*/
#define LOCALHOST           "127.0.0.1"
#define DEFAULT_DESCRIPTOR  "No descriptor available."
#define HTTP_PORT           8165
#define HTTP_PORT_T         8166


/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef UTILS_C
extern int  utilsCompareToServerIP( void );
extern char *utilsGetDescriptor( void );
extern char *utilsHttpPost( char *dest, char *uri, char *post_data, int monitor );
extern char *utilsHttpGet( char *dest, char *uri, int monitor );
extern void utilsHttpDelete( char *dest, char *uri, char *post_data );
extern char *utilsGenUUID( void );
#endif /* !UTILS_C */
#endif /* !UTILS_H */
