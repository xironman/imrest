#!/bin/bash -p
# ------------------------------------------------------
REACHABLE=""

#--------------------------------------------------------------
# Functions
#--------------------------------------------------------------
# Provide command line usage assistance
function doHelp
{
    echo ""
    echo "$0 [-r] "
    echo "where"
    echo "-r    Enable show-reachable"
    echo "-a    Enable show all leak kinds"
    echo "-v    "
    echo ""
    echo "Uses Valgrind to run the app binary in test mode and check for memory leaks."
    echo "Should be run first, followed by unit tests."
    echo "Use Ctrl-C to stop the test and get leak reports from Valgrind."
    echo ""
}

#--------------------------------------------------------------
# Read command line arguments
#--------------------------------------------------------------
REACHABLE=
VERBOSE=
while getopts ":arv" Option
do
    case $Option in
    r)  REACHABLE="--show-reachable=yes";;
    a)  REACHABLE="--show-leak-kinds=all";;
    v)  VERBOSE="-v";;
    *)  doHelp; exit 0;;
    esac
done

set -x
valgrind --tool=memcheck --leak-check=full $VERBOSE $REACHABLE --leak-resolution=high \
    --num-callers=20 --suppressions=tests/gtk.suppression \
    src/imrest -T -v3 -w data/etc
set +x
